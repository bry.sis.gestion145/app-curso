import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-directiva',
  templateUrl: './directiva.component.html',
  styleUrls: ['./directiva.component.less']
})
export class DirectivaComponent implements OnInit {
  /* Linea 2 */
  habilitar: boolean = true;
  /* Linea 1 */
  listaCurso: string[] = ['TypeScript', 'JavaScript', 'Java SE', 'C#', 'PHP'];

  constructor() { }

  ngOnInit(): void {
  }
  /* Linea 3 */
  setHabilitar(): void {
      if(this.habilitar == true){
          this.habilitar = false;
      }else {
          this.habilitar = true;
      }
      // this.habilitar = (this.habilitar == true)? false : true;
  }
  /* Linea 4 */
  printHabilitar(): string {
      if(this.habilitar == true){
         return 'Ocultar';
      }else {
         return 'Mostrar';
      }
      // (this.habilitar==true)? 'Ocultar': 'Mostrar';
  }

}

/* Tarea 12 */
  /* Linea 1: Crear arreglo de cursos */

/* Tarea 15 */
  /* Linea 2: Crear campo habilitar que ocultara o mostrara los cursos */
  /* Linea 3: Crear metodo para asignar valor a la variable habilitar */
  /* Linea 4: Crear metodo para imprimir valor de la variable habilitar */
