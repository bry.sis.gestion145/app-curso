import { Injectable } from '@angular/core';
/* Linea 4 */
import { EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  /* Linea 1 */
  modal: boolean = false;

  /* Linea 5 */
  private _notificarUpload = new EventEmitter<any>();

  constructor() { }

  /* Linea 2 */
  abrirModal(){
     this.modal = true;
  }

  /* Linea 3 */
  cerrarModal(){
     this.modal = false;
  }

  /* Linea 6 */
  get notificarUpload(): EventEmitter<any> {
    return this._notificarUpload;
  }

}

/* Tarea 140 */
  /* Linea 1: Creamos el atributo Modal de Tipo Booleano */
  /* Linea 2: Crear el metodo abrirModal() */
  /* Linea 3: Crear el metodo cerrarModal() */

/* Tarea 148 */
  /* Linea 4: Importar el objeto EventEmitter */
  /* Linea 5: Crear el atributo notificarUpload */
  /* Linea 6: Crear metodo get del atributo notificarUpload */
