import { Component, OnInit } from '@angular/core';
/* Linea 1 */
import { Cliente } from '../cliente';
import { ClienteService } from '../cliente.service';
/* Linea 2 */
// import { ActivatedRoute } from '@angular/router';
/* Linea 7 */
import  swal  from 'sweetalert2';
/* Linea 11 */
import { ViewChild, ElementRef } from '@angular/core';
/* Linea 14 */
import { HttpEventType } from '@angular/common/http';
/* Linea 18 */
import { Input } from '@angular/core';
/* Linea 19 */
import { ModalService } from './modal.service';
/* Linea 24 */
import { AuthService } from '../../usuarios/auth.service';
/* Linea 26 */
import { Factura } from '../../facturas/models/factura';
import { FacturaService } from '../../facturas/services/factura.service';

@Component({
  selector: 'detalle-cliente',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.less']
})
export class DetalleComponent implements OnInit {
  /* Linea 4 */
  // cliente: Cliente = new Cliente();
  /* Linea 21 */
  @Input() cliente: Cliente = new Cliente();

  titulo: String = "Detalle del cliente";

  /* Linea 8 */
  private fotoSeleccionada: File = new File([""], "", { type: "image"});
  // private fotoSeleccionada: File = new File([""], "nombreFoto", { type: "image"});

  /* Linea 12 */
  @ViewChild('fileUploader') fileUploader:ElementRef = new ElementRef(null);

  /* Linea 15 */
  progreso: number = 0;

   /* Linea 3 */
  constructor(private clienteService: ClienteService,
              // private activatedRoute: ActivatedRoute,
              /* Linea 21 */
              public modalService: ModalService,
              /* Linea 25 */
              public authService: AuthService,
              /* Linea 27 */
              private facturaService: FacturaService) {}

  ngOnInit(): void {
    // /* Linea 5 */
    // this.activatedRoute.paramMap.subscribe(params => {
    //     // let id: number = +params.get('id');
    //     /* Codigo de la linea de arriba */
    //     let idString = params?.get('id');
    //     let id: number = 0;
    //     if(!idString){
    //       id = 0;
    //     } else{
    //       id = +idString;
    //     }
    //     /* ***************************** */
    //     if(id){
    //       /* Linea 6 */
    //       this.clienteService.getCliente(id).subscribe(cliente => {
    //         this.cliente = cliente;
    //       });
    //     }
    // });
  }

  /* Linea 9 */
  seleccionarFoto(event: any){
     this.fotoSeleccionada = event.target.files[0];
     console.log(this.fotoSeleccionada);
     /* Linea 17 */
     this.progreso = 0;
     if(this.fotoSeleccionada.type.indexOf('image') < 0 ){
         swal.fire('Error Seleccionar Imagen: ', ` El Archivo debe ser del tipo imagen `, 'error');
         this.fotoSeleccionada = new File([""], "", { type: "image"});
         /* Linea 13 */
         this.fileUploader.nativeElement.value = null;
     }
  }

  /* Linea 16 */
  subirFoto(){
    if(this.fotoSeleccionada.name == ""){
         swal.fire('Error Upload: ', ` Debe seleccionar una foto `, 'error');
    }else{
      this.clienteService.subirFoto(this.fotoSeleccionada, this.cliente.id).subscribe(
        event => {
          if(event.type === HttpEventType.UploadProgress){
            if(event.total) {
                this.progreso = Math.round((event.loaded/event.total)*100);
            }
          }else if(event.type === HttpEventType.Response){
             let response: any = event.body;
             this.cliente = response.cliente as Cliente;
             /* Linea 23 *****************************************/
             this.modalService.notificarUpload.emit(this.cliente);
             /***************************************************/
             swal.fire('La foto se ha subido completamente!', response.mensaje, 'success');
             this.fileUploader.nativeElement.value = null;
          }

        }
      );
    }
  }

  /* Linea 10 */
  // subirFoto(){
  //   if(this.fotoSeleccionada.name == ""){
  //        swal.fire('Error Upload: ', ` Debe seleccionar una foto `, 'error');
  //   }else{
  //     this.clienteService.subirFoto(this.fotoSeleccionada, this.cliente.id).subscribe(
  //       cliente => {
  //         this.cliente = cliente;
  //         swal.fire('La foto se ha subido completamente!', `La foto se ha subido con exito: ${this.cliente.foto}`, 'success');
  //         /* Linea 13 */
  //         this.fileUploader.nativeElement.value = null;
  //       }
  //     );
  //   }
  // }

  /* Linea 22 */
  cerrarModal(){
    this.modalService.cerrarModal();
    this.fotoSeleccionada = new File([""], "", { type: "image"});
    this.progreso = 0;
  }

  /* Linea 28 */
  delete(factura: Factura):void {
    swal.fire({
       title: 'Estas seguro?',
       text: `¿Seguro que desea eliminar la factura ${factura.descripcion}?`,
       icon: 'warning',
       showCancelButton: true,
       confirmButtonColor: '#3085d6',
       cancelButtonColor: '#d33',
       confirmButtonText: 'Si, eliminar!',
       cancelButtonText: 'No, cancelar!'
    }).then((result) => {
       if (result.isConfirmed) {
          this.facturaService.delete(factura.id)
          .subscribe(
            response => {
               this.cliente.facturas = this.cliente.facturas.filter(fact => fact !== factura);
               swal.fire(
                  'Factura Eliminada!',
                  'Factura eliminada con exito.',
                  'success'
               )
            }
          );
       }
    })
  }

}

/* Tarea 128 */
  /* Linea 1: Importamos la clase cliente y el ServicioCliente */
  /* Linea 2: Importamos la clase Activated Route */
  /* Linea 3: Inyectamos el ServicioCliente y la clase ActivatedRoute */
  /* Linea 4: Creamos el atributo cliente y el atributo titulo */
  /* Linea 5: Observar el id del cliente para obtenerlo cuando este sea enviado */
  /* Linea 6: Asignar al atributo cliente, el cliente que se obtuvo */

/* Tarea 134 */
  /* Linea 7: Importar sweetalert2 */
  /* Linea 8: Crear el atributo fotoSeleccionada */
  /* Linea 9: Crear el metodo seleccionarFoto($event) */
  /* Linea 10: Crear el metodo subirFoto() */
  /* Para limpiar el input-file*/
  /* Linea 11: Importar ViewChild y ElemetRef de angular/core */
  /* Linea 12: Generar la anotacion de limpieza del Input-File */
  /* Linea 13: Limpiar el control Input-File */

/* Tarea 137 Implementando barra de progreso */
  /* Linea 14: Importar el evento HttpEventType */
  /* Linea 15: Crear atributo progreso de tipo entero */
  /* Linea 16: Modificar el metodo subirFoto para admitir barra de progreso */
  /* Linea 17: Modificar el metodo seleccionarFoto para admitir barra de progreso */

/* Tarea 142 */
  /* Linea 18: Importar la libreria de entrada Input */
  /* Linea 19: Importar el Modal Service */
  /* Linea 20: Inyectar el Modal Service */
  /* Linea 21: Convertir el atributo cliente en una entrada @Input() */
  /* Linea 22: Implementamos el metodo cerrarModal() */

/* Tarea 142 */
  /* Linea 23: LLamar el metodo get notificarUpload() del modalService en el metodo subirFoto() */

/* Tarea 219 */
   /* Linea 24: Importar el servicio authService */
   /* Linea 25: INyectar el servicio authService */

/* Tarea 262 */
   /* Linea 26: Importar la factura y el factura service */
   /* Linea 27: Inyectar el factura service */
   /* Linea 28: Crear el metodo deleteFactura */

/* Tarea 274 */
   /* Linea 29: Hacer publicos auth y modal service */
