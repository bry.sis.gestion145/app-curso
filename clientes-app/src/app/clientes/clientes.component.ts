import { Component, OnInit } from '@angular/core';
/* Linea 1 */
import { Cliente } from '../clientes/cliente';
/* Linea 3 */
// import { CLIENTES } from '../clientes/clientes.json';
/* Linea 5 */
import { ClienteService } from '../clientes/cliente.service';
/* Linea 9 */
import swal from 'sweetalert2';
/* Linea 11 */
import { tap } from 'rxjs/operators';
/* Linea 14 */
import { ActivatedRoute } from '@angular/router';
/* Linea 20 */
import { ModalService } from '../clientes/detalle/modal.service';
/* LInea 24 */
import { AuthService } from '../usuarios/auth.service';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.less']
})
export class ClientesComponent implements OnInit {
  /* Linea 2 */
  clientes: Cliente[] = [];
  // clientes: Cliente[] = [
  //   {id:  1, nombre: 'Bryan',   apellido: 'Gonzalez', email: 'bry.sis.gestion145@gmail.com', createAt: '2021-07-16'},
  //   {id:  2, nombre: 'Jose',    apellido: 'Briones',  email: 'jose.briones@gmail.com',       createAt: '2021-07-15'},
  //   {id:  3, nombre: 'Carlos',  apellido: 'Orellana', email: 'carlos.orellana@gmail.com',    createAt: '2021-07-14'},
  //   {id:  4, nombre: 'Yulissa', apellido: 'Flores',   email: 'yulissa.flores123@gmail.com',  createAt: '2021-07-13'},
  //   {id:  5, nombre: 'Nazaret', apellido: 'Rivas',    email: 'nazaret.rivas856@gmail.com',   createAt: '2021-07-12'},
  //   {id:  6, nombre: 'Daniela', apellido: 'Torres',   email: 'daniela.torres@gmail.com',     createAt: '2021-07-11'},
  //   {id:  7, nombre: 'Jovany',  apellido: 'Torres',   email: 'jovany.torres@gmail.com',      createAt: '2021-07-10'},
  //   {id:  8, nombre: 'Dinia',   apellido: 'Torres',   email: 'dinia.tores456@gmail.com',     createAt: '2021-07-09'},
  //   {id:  9, nombre: 'Ana',     apellido: 'Obando',   email: 'ana.ovando678@gmail.com',      createAt: '2021-07-08'},
  //   {id: 10, nombre: 'Alexis',  apellido: 'Ovando',   email: 'alexis.ovando@gmail.com',      createAt: '2021-07-07'}
  // ];

  /* Linea 17 */
  public paginador: any;

  /* Linea 19 */
  clienteSeleccionado: Cliente = new Cliente();

  /* Linea 6 */
  constructor(private clienteService: ClienteService,
              /* Linea 15 */
              private activatedRoute: ActivatedRoute,
              /* Linea 21 */ /* Linea 26 */
              public modalService: ModalService,
              /* Linea 25 */ /* Linea 26 */
              public authService: AuthService) {

  }

  ngOnInit(): void {
    /* Linea 16 */
    this.activatedRoute.paramMap.subscribe( params => {
      // let page: number = +params.get('page');
      let pageString = params?.get('page');
      let page: number = 0;
      if(!pageString){
        page = 0;
      } else{
        page = +pageString;
      }
      this.clienteService.getClientes(page).pipe(
        // tap(response => {
        //   console.log("Cliente Component: Tap 3");
        //   (response.content as Cliente[]).forEach(cliente => {
        //     console.log(cliente.nombre)
        //   })
        // })
      ).subscribe( response => {
           this.clientes = (response.content as Cliente[]);
           /* Linea 18 */
           this.paginador = response;
      });
    });
    /* Linea 13 */
    // let page = 0;
    // this.clienteService.getClientes(page).pipe(
    //   tap(response => {
    //     console.log("Cliente Component: Tap 3");
    //     (response.content as Cliente[]).forEach(cliente => {
    //       console.log(cliente.nombre)
    //     })
    //   })
    // ).subscribe( response => this.clientes = (response.content as Cliente[]) );

    /* Linea 23 */
    this.modalService.notificarUpload.subscribe(cliente => {
      this.clientes = this.clientes.map(clienteOriginal => {
        if(cliente.id == clienteOriginal.id){
          clienteOriginal.foto = cliente.foto;
        }
        return clienteOriginal;
      });
    });
  }

  // ngOnInit(): void {
  //   /* Linea 4 */
  //   // this.clientes = CLIENTES;
  //   /* Linea 7 */
  //   // this.clientes = this.clienteService.getClientes();
  //   /* Linea 8: Otra forma de escribirlo */
  //   // this.clienteService.getClientes().subscribe(
  //   //   (clientes) => {
  //   //     this.clientes = clientes
  //   //   }
  //   // );
  //   /* Linea 8 *//* Linea 12 */
  //   this.clienteService.getClientes().pipe(
  //     tap(clientes => {
  //       // this.clientes = clientes Se puede hacer esta asignacion en el Tap
  //       // y omitirla del suscribe
  //       console.log("Cliente Component: Tap 3")
  //       clientes.forEach(cliente => {
  //         console.log(cliente.nombre)
  //       })
  //     })
  //   ).subscribe( clientes => this.clientes = clientes );
  // }

  /* Linea 10 */
  delete(cliente: Cliente): void {
    swal.fire({
       title: 'Estas seguro?',
       text: `¿Seguro que desea eliminar al cliente ${cliente.nombre} ${cliente.apellido}?`,
       icon: 'warning',
       showCancelButton: true,
       confirmButtonColor: '#3085d6',
       cancelButtonColor: '#d33',
       confirmButtonText: 'Si, eliminar!',
       cancelButtonText: 'No, cancelar!'
    }).then((result) => {
       if (result.isConfirmed) {
          this.clienteService.delete(cliente.id)
          .subscribe(
            response => {
               this.clientes = this.clientes.filter(cli => cli !== cliente);
               swal.fire(
                  'Cliente Eliminado!',
                  'Cliente eliminado con exito.',
                  'success'
               )
            }
          );
       }
    })
  }

  /* Linea 22 */
  abrirModal(cliente: Cliente){
    this.clienteSeleccionado = cliente;
    this.modalService.abrirModal();
  }

}

/* Tarea 20 */
  /* Linea  1: Importar clase cliente */
  /* Linea  2: Crear un arreglo de clientes */

/* Tarea 25 */
  /* Linea  3: Importar la constante clientes de archivo clientes.json */
  /* Linea  4: Asignar al arreglo de clientes la constante que se importo */

/* Tarea 28 */
  /* Linea  5: Importar el servicio clientes */
  /* Linea  6: Inyectar el servicio clientes */
  /* Linea  7: Asignar al atributo cliente el metodo getClientes() que viene del servicio cliente */

/* Tarea 31 */
   /* Linea  8: Suscribir o registrar el observador que viene del cliente service */

/* Tarea 81 */
   /* Linea  9: Importar la libreria sweetalert2 */
   /* Linea 10: Implementar el metodo delete */

/* Tarea 100 */
   /* Linea 11: Importar el operador tap */
   /* Linea 12: Utilizar el operador tap en el metodo onInit cuando se invoca el metodo getClientes() del service */

/* Tarea 106 */
  /* Linea 13: Crear el metodo clientes pageable */

/* Tarea 108 */
  /* Linea 14: Importar la libreria ActivatedRoute */
  /* Linea 15: Inyectar el objeto ActivatedRoute */
  /* Linea 16: Implementar la observacion del parametro page */

/* Tarea 110 */
  /* Linea 17: Crear atributo paginator de tipo any */
  /* Linea 18: Asignar la respuesta al atributo paginator */

/* Tarea 141 */
  /* Linea 19: Creamos el atributo clienteSeleccionado de tipo Cliente */
  /* Linea 20: Importamos el modal service */
  /* Linea 21: Inyectamos el modal service */
  /* Linea 22: Implementamos el metodo abrirModal() */

/* Tarea 150 */
  /* Linea 23: Suscribir el metodo get notificarUpload del modalService */

/* Tarea 217 */
  /* Linea 24: Importar el servicio authService */
  /* Linea 25: Inyectar el servicio authService */

/* Tarea 273 */
  /* Linea 26: hacer publicos ModalService Y AuthService */
