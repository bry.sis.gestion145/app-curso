import { Component, OnInit } from '@angular/core';
import { Cliente } from './cliente';
/* Linea 5 */
import { ClienteService } from './cliente.service';
/* Linea 6 */
import { Router } from '@angular/router';
/* Linea 9 */
import swal from 'sweetalert2';
/* Linea 11 */
import { ActivatedRoute } from '@angular/router';
/* Linea 19 */
import { Region } from './region';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.less']
})
export class FormComponent implements OnInit {
  /* Linea 2 */
  public titulo: string = "Crear Cliente";

  /* Linea 3 */ /* Linea 24 */ /* Linea */
  public cliente: Cliente = new Cliente();

  /* Linea 16 */
  public errores: string[] = [];

  /* Linea 20 */
  public regiones: Region[] = [];

  /* Linea 7 */
  constructor(private clienteService: ClienteService,
              private router: Router,
              /* Linea 12 */
              private activatedRoute: ActivatedRoute) {

              }


  ngOnInit(): void {
    /* Linea 14 */
    this.cargarCliente();
    /* Linea 22 */
    this.cargarRegiones();
  }

  /* Linea 17 */
  public create(): void {
    this.clienteService.create(this.cliente).subscribe(
        cliente => {
        this.router.navigate(['/clientes'])
        /* Linea 10 */
        swal.fire('Nuevo Cliente', `El cliente: ${cliente.nombre} ha sido creado con exito!`, 'success')
      },
      err => {
        this.errores = err.error.errors as string[];
        console.error('Estado: ' + err.status);
        console.error(err.error.errors);
      }
    );
  }

  /* Linea 4 */
  // public create(): void {
  //   // console.log("Clicked!");
  //   // console.log(this.cliente);
  //   /* Linea 8 */
  //   this.clienteService.create(this.cliente).subscribe(
  //       cliente => {
  //       this.router.navigate(['/clientes'])
  //       /* Linea 10 */
  //       swal.fire('Nuevo Cliente', `El cliente: ${cliente.nombre} ha sido creado con exito!`, 'success')
  //     }
  //   );
  // }

  /* Linea 13 */
  cargarCliente(): void {
     this.activatedRoute.params.subscribe(params => {
       let id = params['id'];
       if(id){
          this.clienteService.getCliente(id).subscribe(
            (cliente) => this.cliente = cliente
          )}
     });
  }

  /* Linea 21 */
  cargarRegiones(): void {
     this.clienteService.getRegiones().subscribe(regiones => {
         this.regiones = regiones
     });
  }

  /* Linea 18 */
  public update():void{
    this.cliente.facturas = null;
    this.clienteService.update(this.cliente)
    .subscribe( cliente => {
      this.router.navigate(['/clientes'])
      swal.fire('Cliente Actualizado', `El cliente: ${cliente.nombre} ha sido actualizado con exito!`, 'success')
    },
    err => {
      this.errores = err.error.errors as string[];
      console.error('Estado: ' + err.status);
      console.error(err.error.errors);
    }
  );
  }

  /* Linea 15 */
  // public update():void{
  //   this.clienteService.update(this.cliente)
  //   .subscribe( cliente => {
  //     this.router.navigate(['/clientes'])
  //     swal.fire('Cliente Actualizado', `El cliente: ${cliente.nombre} ha sido actualizado con exito!`, 'success')
  //   });
  // }

  /* Linea 23 */
  compararRegion(region1: Region, region2: Region): boolean {
    if(region1 === undefined && region2 === undefined){
       return true;
    }
    if(region1 === null || region2 === null){
      return false;
    }else {
      if(region1?.id === region2?.id){
        return true;
      }else {
        return false;
      }
    }
    // Otra forma de escribir este mismo codigo
    // return region1 === null || region2 === null ? false : region1.id === region2.id;
    // Si tira error de indefinido podemos ser menos estrictos al hacer la comparacion
    // return region1 == null || region2 == null ? false : region1.id == region2.id;
    // return region1 === null || region2 === null || region1 === undefined || region2 === undefined ? false : region1.id === region2.id;
  }

}

/* Tarea 65 */
  /* Linea  1: Importar clase cliente */
  /* Linea  2: Crear atributo titulo */
  /* Linea  3: Crear atributo cliente */
  /* Linea  4: Crear el metodo create() */

/* Tarea 70 */
  /* Linea  5: Importar la clase service */
  /* Linea  6: Importar la clase router */
  /* Linea  7: Inyectar la clase service y la clase router */
  /* Linea  8: Implementar el metodo create */

/* Tarea 72 */
  /* Linea  9: Importar la libreria sweetalert2 */
  /* Linea 10: Crear la alerta sweetalert2 */

/* Tarea 74 */
  /* Linea 11: Importar la libreria ActivatedRoute */
  /* Linea 12: Inyectar la libreria ActivatedRoute */
  /* Linea 13: Crear el metodo cargarCliente() */
  /* Linea 14: Llamar al metodo cargarCliente() */

/* Tarea 78 */
  /* Linea 15: Crear el metodo update */

/* Tarea 92 */
  /* Linea 16: Crear un arreglo de errores */
  /* Linea 17: Capturar errores en el metodo create() */
  /* Linea 18: Capturar errores en el metodo update() */

/* Tarea 169 */
  /* Linea 19: Importar la clase Region */
  /* Linea 20: Crear el atributo array de tipo region */
  /* Linea 21: Suscribir el metodo getRegiones() que tenemos en el service, en un metodo llamado cargarRegiones() */
  /* Linea 22: Hacer la llamada del metodo cargarRegiones() en el constructor */

/* Tarea 171 */
  /* Linea 23: Implementar el metodo compararRegiones */

/* Tarea 274 /*
  /* Linea 24: Cambiar el atributo cliente a public  */

/* Tarea 275 /*
  /* Linea 25: Cambiar el arreglo clientes a publico */
