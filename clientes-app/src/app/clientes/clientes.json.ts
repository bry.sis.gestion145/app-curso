/* Linea 1 */
import { Cliente } from '../clientes/cliente';

/* Linea 2 */
export const CLIENTES: Cliente[] = [
  {id:  1, nombre: 'Bryan',   apellido: 'Gonzalez', email: 'bry.sis.gestion145@gmail.com', createAt: '2021-07-16'},
  {id:  2, nombre: 'Jose',    apellido: 'Briones',  email: 'jose.briones@gmail.com',       createAt: '2021-07-15'},
  {id:  3, nombre: 'Carlos',  apellido: 'Orellana', email: 'carlos.orellana@gmail.com',    createAt: '2021-07-14'},
  {id:  4, nombre: 'Yulissa', apellido: 'Flores',   email: 'yulissa.flores123@gmail.com',  createAt: '2021-07-13'},
  {id:  5, nombre: 'Nazaret', apellido: 'Rivas',    email: 'nazaret.rivas856@gmail.com',   createAt: '2021-07-12'},
  {id:  6, nombre: 'Daniela', apellido: 'Torres',   email: 'daniela.torres@gmail.com',     createAt: '2021-07-11'},
  {id:  7, nombre: 'Jovany',  apellido: 'Torres',   email: 'jovany.torres@gmail.com',      createAt: '2021-07-10'},
  {id:  8, nombre: 'Dinia',   apellido: 'Torres',   email: 'dinia.tores456@gmail.com',     createAt: '2021-07-09'},
  {id:  9, nombre: 'Ana',     apellido: 'Obando',   email: 'ana.ovando678@gmail.com',      createAt: '2021-07-08'},
  {id: 10, nombre: 'Alexis',  apellido: 'Ovando',   email: 'alexis.ovando@gmail.com',      createAt: '2021-07-07'}
];

/*
    INSERT INTO clientes (nombre, apellido, email, create_at) VALUES ('Bryan',   'Gonzalez', 'bry.sis.gestion145@gmail.com', '2021-07-16');
    INSERT INTO clientes (nombre, apellido, email, create_at) VALUES ('Jose',    'Briones',  'jose.briones@gmail.com',       '2021-07-15');
    INSERT INTO clientes (nombre, apellido, email, create_at) VALUES ('Carlos',  'Orellana', 'carlos.orellana@gmail.com',    '2021-07-14');
    INSERT INTO clientes (nombre, apellido, email, create_at) VALUES ('Yulissa', 'Flores',   'yulissa.flores123@gmail.com',  '2021-07-13');
    INSERT INTO clientes (nombre, apellido, email, create_at) VALUES ('Nazaret', 'Rivas',    'nazaret.rivas856@gmail.com',   '2021-07-12');
    INSERT INTO clientes (nombre, apellido, email, create_at) VALUES ('Daniela', 'Torres',   'daniela.torres@gmail.com',     '2021-07-11');
    INSERT INTO clientes (nombre, apellido, email, create_at) VALUES ('Jovany',  'Torres',   'jovany.torres@gmail.com',      '2021-07-10');
    INSERT INTO clientes (nombre, apellido, email, create_at) VALUES ('Dinia',   'Torres',   'dinia.tores456@gmail.com',     '2021-07-09');
    INSERT INTO clientes (nombre, apellido, email, create_at) VALUES ('Ana',     'Obando',   'ana.ovando678@gmail.com',      '2021-07-08');
    INSERT INTO clientes (nombre, apellido, email, create_at) VALUES ('Alexis',  'Ovando',   'alexis.ovando@gmail.com',      '2021-07-07');
*/

/* Tarea 24 */
  /* Linea 1: Importar clase clientes */
  /* Linea 2: Crear una constante exportable clientes */
