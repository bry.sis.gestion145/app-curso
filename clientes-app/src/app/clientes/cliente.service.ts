import { Injectable } from '@angular/core';
/* Linea 1 */
// import { CLIENTES } from '../clientes/clientes.json';
/* Linea 2 */
import { Cliente } from '../clientes/cliente';
/* Linea 4 */
import { Observable } from 'rxjs';
// import { of } from 'rxjs';
/* Linea 7 */
import { HttpClient } from '@angular/common/http';
/* Linea 11 */
import { map } from 'rxjs/operators';
/* Linea 14 */
import { HttpHeaders } from '@angular/common/http';
/* Linea 19 */
import { catchError } from 'rxjs/operators';
/* Linea 20 */
import { throwError } from 'rxjs';
/* Linea 21 */
// import swal from 'sweetalert2';
/* Linea 22 */
import { Router } from '@angular/router';
/* Linea 32 */
// import { formatDate, DatePipe } from '@angular/common';
/* Linea 35 */
// import localeES from '@angular/common/locales/es';
// import { registerLocaleData } from '@angular/common';
/* Linea 37 */
import { tap } from 'rxjs/operators';
/* Linea 41 */
import { HttpRequest } from '@angular/common/http';
/* Linea 42 */
import { HttpEvent } from '@angular/common/http';
/* Linea 44 */
import { Region } from './region';
/* Linea 48 */
import { AuthService } from '../usuarios/auth.service';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {
  /* Linea 9 */
  private urlEndPoint: string = 'http://localhost:8080/api/clientes';
  /* Linea 15 */
  // private httpHeaders = new HttpHeaders({'Content-Type': 'application/json'});
  /* Linea 8 */

  nombre: string = "";
  constructor(private http: HttpClient,
              /* Linea 23 */
              private router: Router,
              /* Linea 49 */
              private auth: AuthService) {}

  /* Linea 50 */
  // private agregarAuthorizationHeader(){
  //   let token = this.auth.token;
  //   if(token != null){
  //     return this.httpHeaders.append('Authorization', 'Bearer ' + token);
  //   }
  //   return this.httpHeaders;
  // }

  /* Linea 46 */
  // private isNotAutorizado(error): boolean {
  //   if(error.status==401){
  //       /* Linea 52 */
  //       if(this.auth.isAuthenticated()){
  //            this.auth.logout();
  //       }
  //       this.router.navigate(['/login']);
  //       return true;
  //   }
  //   if(error.status==403){
  //       swal.fire('Acceso denegado', `Hola ${this.auth.usuario.username}, no tienes acceso a este recurso!`, 'warning');
  //       this.router.navigate(['/clientes']);
  //       return true;
  //   }
  //   return false;
  // }

  /* Linea 39 */
  getClientes(page: number): Observable<any> {
    // return this.http.get(this.urlEndPoint +'/page/'+ page).pipe(
    //   tap( (response: any) => {
    //       console.log("Cliente Service: Tap 1");
    //       (response.content as Cliente[]).forEach(cliente => {
    //         console.log(cliente.nombre)
    //       });
    //   }),
    //   map( (response: any) => {
    //     (response.content as Cliente[]).map(cliente => {
    //       cliente.nombre = cliente.nombre.toUpperCase();
    //       return cliente;
    //     });
    //     return response;
    //   }),
    //   tap( response => {
    //       console.log("Cliente Service: Tap 2");
    //       (response.content as Cliente[]).forEach(cliente => {
    //         console.log(cliente.nombre)
    //       });
    //   })
    // );
    /* Linea 53 */
    return this.http.get(this.urlEndPoint +'/page/'+ page);
  }

  /* Linea 5 */
  // getClientes(): Observable<Cliente[]> {
  //   /* Linea 6 */
  //   // return of(CLIENTES);
  //   /* Linea 10 */
  //   // return this.http.get<Cliente[]>(this.urlEndPoint);
  //   /* Linea 12 */
  //   // return this.http.get(this.urlEndPoint).pipe(
  //   //   map( response => response as Cliente[])
  //   // );
  //   /* Linea 12: Otra forma de hacerlo*/
  //   // return this.http.get(this.urlEndPoint).pipe(
  //   //   map( function(response) {
  //   //          return response as Cliente[]
  //   //     })
  //   // );
  //   /* Linea 30 */
  //   return this.http.get(this.urlEndPoint).pipe(
  //     /* Linea 38 */
  //     tap( response => {
  //         console.log("Cliente Service: Tap 1")
  //         let clientes = response as Cliente[];
  //         clientes.forEach(cliente => {
  //           console.log(cliente.nombre)
  //         });
  //     }),
  //     map( response => {
  //       let clientes = response as Cliente[];
  //       return clientes.map(cliente => {
  //         /* Linea 31 */
  //         cliente.nombre = cliente.nombre.toUpperCase();
  //         /* Linea 36 */
  //         // registerLocaleData(localeES, 'es');
  //         /* Linea 33 */
  //         // cliente.createAt = formatDate(cliente.createAt, 'EEEE dd, MMMM yyyy', 'es');
  //         /* Linea 34 Esta linea no funciono */
  //         // let datePipe = new DatePipe('es');
  //         // cliente.createAt = datePipe.transform(cliente.createAt, 'EEEE dd, MMMM yyyy');
  //         return cliente;
  //       });
  //     }),
  //     /* Linea 38 */
  //     tap( clientes => {
  //         /* Ya no es necesaria porque el map hace la conversion y el tap recibe los datos ya convertidos */
  //         // let clientes = response as Cliente[];
  //         console.log("Cliente Service: Tap 2")
  //         clientes.forEach(cliente => {
  //           console.log(cliente.nombre)
  //         });
  //     })
  //   );
  // }

  /* Linea 3 */
  // getClientes(): Cliente[] {
  //   return CLIENTES;
  // }

  /* Linea 28 */
  create(cliente: Cliente): Observable<Cliente>{
    // return this.http.post(this.urlEndPoint, cliente, {headers: this.agregarAuthorizationHeader()}).pipe(
    return this.http.post(this.urlEndPoint, cliente).pipe(
      map((response: any) => response.cliente as Cliente),
      catchError(error => {

        /* Linea 47 */ /* Linea 53: quitamos codigo */
        // if(this.isNotAutorizado(error)){;
        //   return throwError(error);
        // }

        if(error.status == 400){
          return throwError(error);
        }

        // console.error(error.error.mensaje);
        /* Linea 53 */
        if(error.error.mensaje){
          console.error(error.error.mensaje);
        }
        /* Linea 53: quitamos codigo */
        // swal.fire(error.error.mensaje, error.error.Error, 'error');
        return throwError(error);
      })
    );
  }

  /* Linea 25 */
  // create(cliente: Cliente): Observable<Cliente>{
  //   return this.http.post(this.urlEndPoint, cliente, {headers: this.httpHeaders}).pipe(
  //     map((response: any) => response.cliente as Cliente),
  //     catchError(e => {
  //       console.error(e.error.mensaje);
  //       swal.fire(e.error.mensaje, e.error.Error, 'error');
  //       return throwError(e);
  //     })
  //   );
  // // Manejo del retorno: https://www.udemy.com/course/angular-spring/learn/lecture/11338894#announcements
  // }

  /* Linea 13 */
  // create(cliente: Cliente): Observable<Cliente>{
  //   return this.http.post<Cliente>(this.urlEndPoint, cliente, {headers: this.httpHeaders});
  // }

  /* Linea 24 */ /* Linea 51 */
  getCliente(id: number): Observable<Cliente>{
    // return this.http.get<Cliente>(`${this.urlEndPoint}/${id}`, {headers: this.agregarAuthorizationHeader()}).pipe(
    return this.http.get<Cliente>(`${this.urlEndPoint}/${id}`).pipe(
       catchError(error => {

          /* Linea 47 */ /* Linea 53: quitamos codigo */
          // if(this.isNotAutorizado(error)){;
          //   return throwError(error);
          // }
          /* Linea 53 */
          if(error.status != 401 && error.error.mensaje){
            this.router.navigate(['/cliente']);
            console.error(error.error.mensaje);
          }
          // this.router.navigate(['/cliente']);
          // console.error(error.error.mensaje);
          /* Linea 53: quitamos codigo */
          // swal.fire('Error al obtener el cliente', error.error.Error, 'error');
          return throwError(error);
       })
    );
  }

  /* Linea 16 */
  // getCliente(id: number): Observable<Cliente>{
  //   return this.http.get<Cliente>(`${this.urlEndPoint}/${id}`);
  // }

  /* Linea 29 */ /* Linea 51 */
  update(cliente: Cliente): Observable<Cliente>{
    // return this.http.put<any>(`${this.urlEndPoint}/${cliente.id}`, cliente, {headers: this.agregarAuthorizationHeader()}).pipe(
    return this.http.put<any>(`${this.urlEndPoint}/${cliente.id}`, cliente).pipe(
      map((response: any) => response.cliente as Cliente),
      catchError(error => {

        /* Linea 47 */ /* Linea 53: Quitamos codigo */
        // if(this.isNotAutorizado(error)){;
        //   return throwError(error);
        // }

        if(error.status == 400){
             return throwError(error);
         }
         /* Linea 53 */
         if(error.error.mensaje){
           console.error(error.error.mensaje);
         }
        // console.error(error.error.mensaje);
        /* Linea 53: Quitamos codigo */
        // swal.fire(error.error.mensaje, error.error.Error, 'error');
        return throwError(error);
      })
    );
  }

  /* Linea 26 */
  // update(cliente: Cliente): Observable<Cliente>{
  //   return this.http.put<any>(`${this.urlEndPoint}/${cliente.id}`, cliente, {headers: this.httpHeaders}).pipe(
  //     map((response: any) => response.cliente as Cliente),
  //     catchError(e => {
  //       console.error(e.error.mensaje);
  //       swal.fire(e.error.mensaje, e.error.Error, 'error');
  //       return throwError(e);
  //     })
  //   );
  // }

  /* Linea 17 */
  // update(cliente: Cliente): Observable<Cliente>{
  //   return this.http.put<Cliente>(`${this.urlEndPoint}/${cliente.id}`, cliente, {headers: this.httpHeaders});
  // }

  /* Linea 27 */ /* Linea 51 */
  delete(id: number): Observable<Cliente> {
    // return this.http.delete<Cliente>(`${this.urlEndPoint}/${id}`, {headers: this.agregarAuthorizationHeader()}).pipe(
    return this.http.delete<Cliente>(`${this.urlEndPoint}/${id}`).pipe(
      catchError(error => {

        /* Linea 47 */ /* Linea 53: Quitamos codigo */
        // if(this.isNotAutorizado(error)){;
        //   return throwError(error);
        // }

        // console.error(error.error.mensaje);
        /* Linea 53 */
        if(error.error.mensaje){
          console.error(error.error.mensaje);
        }
        /* Linea 53: Quitamos codigo */
        // swal.fire(error.error.mensaje, error.error.Error, 'error');
        return throwError(error);
      })
    );
  }

  /* Linea 18 */
  // delete(id: number): Observable<Cliente> {
  //   return this.http.delete<Cliente>(`${this.urlEndPoint}/${id}`, {headers: this.httpHeaders});
  // }

  /* Linea 43 */
  subirFoto(archivo: File, id: any): Observable<HttpEvent<{}>> {
    let formData = new FormData();
    formData.append("archivo", archivo);
    formData.append("id", id);

    /* Linea 51 */
    // let httpHeaders = new HttpHeaders();
    // let token = this.auth.token;
    // if(token != null){
    //     httpHeaders = httpHeaders.append('Authorization', 'Bearer ' + token);
    // }

    const req = new HttpRequest('POST', `${this.urlEndPoint}/upload`, formData, {
      reportProgress: true,
      // headers: httpHeaders
    });

    // return this.http.request(req);
    /* Linea 47 */
    // return this.http.request(req).pipe(
    //   catchError(error => {
    //     this.isNotAutorizado(error);
    //     return throwError(error);
    //   })
    // );
    /* Linea 53 */
    return this.http.request(req);
  }

  /* Linea 40 */
  // subirFoto(archivo: File, id: any): Observable<Cliente> {
  //     let formData = new FormData();
  //     formData.append("archivo", archivo);
  //     formData.append("id", id);
  //     return this.http.post(`${this.urlEndPoint}/upload`, formData).pipe(
  //         map((response: any) => { return response.cliente as Cliente }),
  //         catchError(e => {
  //             console.error(e.error.mensaje);
  //             swal.fire(e.error.mensaje, e.error.Error, 'error');
  //             return throwError(e);
  //         })
  //     );
  // }

  /* Linea 45 */
  getRegiones():Observable<Region[]> {
     // return this.http.get<Region[]>(this.urlEndPoint + '/regiones');
     /* Linea 47 */ /* Linea 51 */
     // return this.http.get<Region[]>(this.urlEndPoint + '/regiones', {headers: this.agregarAuthorizationHeader()}).pipe(
     // return this.http.get<Region[]>(this.urlEndPoint + '/regiones').pipe(
     //   catchError(error => {
     //     this.isNotAutorizado(error);
     //     return throwError(error);
     //   })
     // );
     /* Linea 53 */
     return this.http.get<Region[]>(this.urlEndPoint + '/regiones');
  }

}

/* Tarea 27 */
  /* Linea  1: Importar constante clientes del archivo clientes.json */
  /* Linea  2: Importar clase cliente */
  /* Linea  3: Crear metodo getClientes */

/* Tarea 30 */
  /* Linea  4: Importar el API observable y el operador of */
  /* Linea  5: Cambiar la declaracion del retorno del metodo getClientes a un observable */
  /* Linea  6: Convertir el retorno del metodo getClientes a observable */

/* Tarea 58 */
  /* Linea  7: Importar el modulo HttpClient */
  /* Linea  8: Inyectar el modulo HttpClient */
  /* Linea  9: Definir una url al back end la cual traera los datos del mismo */
  /* Linea 10: Returnar en el metodo get clientes una peticion get al back end */

/* Tarea 58: Otro metodo de hacerlo */
  /* Linea 11: Importar el operador map */
  /* Linea 12: Returnar en el metodo get clientes una peticion get al back end */

/* Tarea 69 */
  /* Linea 13: Crear el metodo create() */
  /* Linea 14: Importar las cabeceras */
  /* Linea 15: Crear atributo con las cabeceras */

/* Tarea 73 */
  /* Linea 16: Crear el metodo getClientes() */

/* Tarea 77 */
  /* Linea 17: Crear el metodo update() */

/* Tarea 78 */
  /* Linea 18: Crear el metodo delete() */

/* Tarea 87 */
  /* Linea 19: Importar el operador para capturar errores CatchError */
  /* Linea 20: Importar el operador throwError */
  /* Linea 21: Importar la libreria sweetalert */
  /* Linea 22: Importar la libreria Router */
  /* Linea 23: Inyectar el objeto Router */
  /* Linea 24: Capturar el error que viene del back-end en el metodo getCliente() */
  /* Linea 25: Capturar el error que viene del back-end en el metodo create() */
  /* Linea 26: Capturar el error que viene del back-end en el metodo update() */
  /* Linea 27: Capturar el error que viene del back-end en el metodo delete() */

/* Tarea 91 */
  /* Linea 28: Capturar errores del hasErrors en el metodo create() */
  /* Linea 29: Capturar errores del hasErrors en el metodo update() */

/* Tarea 95 */
  /* Linea 30: Manipular el retorno de getClientes() para tratar cada elemento del arreglo */
  /* Linea 31: Convertir a mayusculas el nombre de los clientes */
  /* Linea 32: Importar la libreria formatDate y la instacia DatePipe*/
  /* Linea 33: Darle formato a la fecha de los clientes */
  /* Linea 34: Darle formato a la fecha de los clientes Otro metodo */
  /* Linea 35: Importar la libreria localeES y la funcion registerLocaleData */
  /* Linea 36: Registrar nuestro locale */

/* Tarea 99 */
  /* Linea 37: Importar el operador tap */
  /* Linea 38: Utilizar el operador tap en el metodo getClientes() */

/* Tarea 105 */
  /* Linea 39: Implementar metodo getClientes() paginable */

/* Tarea 133 */
  /* Linea 40: Implementar el metodo upload() */

/* Tarea 136 Implementando barra de progreso */
  /* Linea 41: Importar la clase HttpRequest */
  /* Linea 42: Importar el evento HttpEvent */
  /* Linea 43: Modificar el metodo subirFoto para que lleve barra de progreso */

/* Tarea 168 */
  /* Linea 44: Importar la clase Regiones */
  /* Linea 45: Implementar el metodo getRegiones() */

/* Tarea 207 */
  /* Linea 46: Implementar metodo para restringir los accesos */
  /* Linea 47: Hacemos el llamado del metodo no autorizado en todos los metodos que acceden a recursos */

/* Tarea 217 */
  /* Linea 48: Importar el servicio AuthService */
  /* Linea 49: Inyectar el servicio AuthService */
  /* Linea 50: Crear metodo para agregar autorizaciones en las cabeceras http */
  /* Linea 51: Llamada al metodo agregar autorizaciones cada vez que se accede a un recurso */

/* Tarea 221 */
  /* Linea 52: Validar la expiracion del token */

/* Tarea 234 */
  /* Linea 53: Utilizar el AuthInterceptor para manejar el error en los metodos */
