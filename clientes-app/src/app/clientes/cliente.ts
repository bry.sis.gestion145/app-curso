/* Linea 3 */
import { Region } from './region';
/* Linea 5 */
import { Factura } from '../facturas/models/factura';

export class Cliente {
   /* Linea 1 */
   id: number = 0;
   nombre: string = "";
   apellido: string = "";
   createAt: string = "";
   email: string = "";
   /* Linea 2 */
   foto: string = "";
   /* Linea 4 */
   region: Region;
   /* Linea 6 */
   facturas: Factura[] = [];
}

/* Tarea 19 */
  /* Linea 1: Crear los atributos de la clase cliente */

/* Tarea 132 */
  /* Linea 2: Crear el atributo foto */

/* Tarea 166 */
  /* Linea 3: Importar la clase Region */
  /* Linea 4: Crear el atributo region */

  /* Tarea 166 */
    /* Linea 3: Importar la clase Region */
