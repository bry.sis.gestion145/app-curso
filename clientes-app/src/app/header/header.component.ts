import { Component, OnInit } from '@angular/core';
/* Linea 2 */
import { AuthService } from '../usuarios/auth.service';
/* Linea 5 */
import { Router } from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less']
})
export class HeaderComponent implements OnInit {
  /* Linea 1 */
  title: string = 'App Angular Spring';

  /* Linea 3 */ /* Linea 7 */
  constructor(public auth: AuthService,
              /* Linea 6 */
              public router: Router) { }

  ngOnInit(): void {
  }

  logout() : void {
    let username: string = this.auth.usuario.username;
    this.auth.logout();
    swal.fire('Logout', `Hola ${username}, has cerrado session!`, 'success');
    this.router.navigate(['/login']);
  }

}

/* Tarea 3 */
  /* Linea 1: Definir el atributo titulo del encabezado */

/* Tarea 215 */
  /* Linea 2: Importamos el servicio AuthService */
  /* Linea 3: Inyectamos el servicio AuthService */
  /* Linea 4: Implementamos el metodo logout() */

  /* Linea 5: Importamos el router y sweetalert */
  /* Linea 6: Inyectamos el router */

/* Tarea 275 */
  /* Linea 7: Hacemos el auth service como public */
