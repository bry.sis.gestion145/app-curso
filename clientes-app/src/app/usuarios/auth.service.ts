import { Injectable } from '@angular/core';
/* Linea 1 */
import { Observable } from 'rxjs';
import { Usuario } from './usuario';
/* Linea 2 */
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  /* Linea 5 */
  private _usuario: Usuario;
  private _token: string;

  /* Linea 3 */
  constructor(private http: HttpClient) { }

  /* Linea 4 */
  login(usuario: Usuario): Observable<any> {
     const urlEndPoint = 'http://localhost:8080/oauth/token';
     const credenciales = btoa('angularapp' + ':' + '12345');

     const httpHeaders = new HttpHeaders({
       'Content-Type': 'application/x-www-form-urlencoded',
       'Authorization': 'Basic ' + credenciales
     });

     let params = new URLSearchParams();
     params.set('grant_type', 'password');
     params.set('username', usuario.username);
     params.set('password', usuario.password);

     return this.http.post(urlEndPoint, params.toString(), {headers: httpHeaders});
  }

  /* Linea 11 */
  logout(): void {
      this._token = null;
      this._usuario = null;
      //  sessionStorage.clear();
      sessionStorage.removeItem('usuario');
      sessionStorage.removeItem('token');
  }

  /* Linea 7 */
  guardarUsuario(accessToken: string) {
     let payload = this.obtenerDatosToken(accessToken);
     this._usuario = new Usuario();
     this._usuario.nombre = payload.nombre;
     this._usuario.apellido = payload.apellido;
     this._usuario.email = payload.email;
     this._usuario.username = payload.user_name;
     this._usuario.roles = payload.authorities;
     sessionStorage.setItem('usuario', JSON.stringify(this._usuario));
  }
  /* Linea 8 */
  guardarToken(accessToken: string) {
     this._token = accessToken;
     sessionStorage.setItem('token', this._token);
  }

  /* Linea 6 */
  obtenerDatosToken(accessToken: string): any {
    if(accessToken != null) {
       return JSON.parse(atob(accessToken.split(".")[1]));
    }
    return null;
  }

  /* Linea 9 */
  public get usuario(): Usuario {
    if(this._usuario != null) {
      return this._usuario;
    } else if(this._usuario == null && sessionStorage.getItem('usuario') != null){
       this._usuario = JSON.parse(sessionStorage.getItem('usuario')) as Usuario;
       return this._usuario;
    }
    return new Usuario();
  }

  /* Linea 9 */
  public get token(): string {
    if(this._token != null) {
      return this._token;
    } else if(this._token == null && sessionStorage.getItem('token') != null){
       this._token = sessionStorage.getItem('token') as string;
       return this._token;
    }
    return null;
  }

  /* Linea 10 */
  isAuthenticated(): boolean {
     let payload = this.obtenerDatosToken(this.token); // LLamando al metodo get Token
     if(payload != null && payload.user_name != "" && payload.user_name.length > 0){
        return true;
     }
     return false;
  }

  /* Linea 12 */
  hasRole(role: string): boolean{
     if(this.usuario.roles.includes(role)){
       return true;
     }
     return false;
  }

}

/* Tarea 213 */
  /* Linea 1: Importar el objeto Observable y la clase Usuario */
  /* Linea 2: Importar el objeto HttpClient y HttpHeaders */
  /* Linea 3: Inyectar el objeto HttpClient */
  /* Linea 4: Implementar el metodo login() */
  /* Linea 5: Crear los atributos accesables _usuario y _token */
  /* Linea 6: Implementar metodo obtener datos de token */
  /* Linea 7: Implementar el metodo guardarUsuario() */
  /* Linea 8: Implementar el metodo guardarToken() */
  /* Linea 9: Implementar los metodos gets y sets del atributo usuario */
  /* Linea 10: Implementar metodo para confirmar si esta autenticado el usuario */
  /* Linea 11: Implementamos el metodo cerrar sesion */
  /* Linea 12: Implementar el metodo hasRole() */
