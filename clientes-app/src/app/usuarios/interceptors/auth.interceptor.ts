/* Linea 1 */
import { Injectable } from '@angular/core';
import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest
} from '@angular/common/http';
/* Linea 2 */
import swal from 'sweetalert2';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { Router } from '@angular/router';
import { AuthService } from '../../usuarios/auth.service';

import { Observable } from 'rxjs';

/** Pass untouched request through to the next request handler. */
@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  /* Linea 3 */
  constructor(private router: Router,
              private auth: AuthService){

  }

  intercept(req: HttpRequest<any>, next: HttpHandler):
    Observable<HttpEvent<any>> {
       /* Linea 4 */
       return next.handle(req).pipe(
         catchError(error => {
               if(error.status == 401){
                   if(this.auth.isAuthenticated()){
                        this.auth.logout();
                   }
                   this.router.navigate(['/login']);
               }
               if(error.status == 403){
                   swal.fire('Acceso denegado', `Hola ${this.auth.usuario.username}, no tienes acceso a este recurso!`, 'warning');
                   this.router.navigate(['/clientes']);
               }
               return throwError(error);
         })
       );
    }
}


/* Tarea 233 */
  /* Linea 1: Implementamos el interceptor: https://angular.io/guide/http#intercepting-requests-and-responses */
  /* Linea 2: Importar el sweetalert, el catchError, el throwError, el router y el authService */
  /* Linea 3: Inyectar el objeto router */
  /* Linea 4: Manejar los errores con el metodo pipe */
