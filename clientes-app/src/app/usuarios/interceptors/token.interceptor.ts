/* Linea 1 */
import { Injectable } from '@angular/core';
import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest
} from '@angular/common/http';
/* Linea 2 */
import { AuthService } from '../../usuarios/auth.service';

import { Observable } from 'rxjs';

/** Pass untouched request through to the next request handler. */
@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  /* Linea 3 */
  constructor(private auth: AuthService){

  }

  intercept(req: HttpRequest<any>, next: HttpHandler):
    Observable<HttpEvent<any>> {
      /* Linea 4 */
      let token = this.auth.token;
      if(token != null){
         const authReq = req.clone({
           headers: req.headers.set('Authorization', 'Bearer ' + token)
         });
         return next.handle(authReq);
      }
      return next.handle(req);
    }
}


/* Tarea 230 */
  /* Linea 1: Implementamos el interceptor: https://angular.io/guide/http#intercepting-requests-and-responses */
  /* Linea 2: Importar el AuthService  */
  /* Linea 3: Inyectar el AuthService  */
  /* Linea 4: Implementamos el interceptor */
