import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
/* Linea 1 */
import { AuthService } from '../../usuarios/auth.service';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  /* Linea 2*/
  constructor(private auth: AuthService,
              private router: Router){

  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    /* Linea 3 */
    if(this.auth.isAuthenticated()){
        /* Linea 6 */
        if(this.isTokenExpirado()){
           this.auth.logout();
           this.router.navigate(['/login']);
           return false;
        }
        return true;
    }
    /* Linea 4 */
    this.router.navigate(['/login']);
    return false;
  }

  /* Linea 5 */
  isTokenExpirado(): boolean{
    let token = this.auth.token;
    let payload = this.auth.obtenerDatosToken(token);
    let now = new Date().getTime()/1000;
    if(payload.exp < now){
      return true;
    }
    return false;
  }

}

/* Tarea 223 */
  /* Linea 1: Importar el auth service y el router */
  /* Linea 2: Inyectar el auth service y el router */
  /* Linea 3: Implementar el metodo de validacion */
  /* Linea 4: Redireccionar al Login */

/* Tarea 228 */
  /* Linea 5: Crear el metodo para validar si el token ha expirado */
  /* Linea 6: Validamos si el token a expirado */
