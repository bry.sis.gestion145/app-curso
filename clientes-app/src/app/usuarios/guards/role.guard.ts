import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
/* Linea 1 */
import { AuthService } from '../../usuarios/auth.service';
import { Router } from '@angular/router';
/* Linea 3 */
import swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate {

  /* Linea 2*/
  constructor(private auth: AuthService,
              private router: Router){

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

      /* Linea 4 */
      if(!this.auth.isAuthenticated()){
          this.router.navigate(['/login']);
          return false;
      }
      /* Linea 5 */
      let role = next.data['role'] as string;
      console.log(role);
      if(this.auth.hasRole(role)){
          return true;
      }
      /* Linea 6 */
      swal.fire('Acceso denegado', `Hola ${this.auth.usuario.username} no tienes acceso!`,'warning');
      this.router.navigate(['/clientes']);
      return false;
  }

}

/* Tarea 226 */
  /* Linea 1: Importar el auth service y el router */
  /* Linea 2: Inyectar el auth service y el router */
  /* Linea 3: Importar el sweetalert */
  /* Linea 4: Validamos que este autenticado */
  /* Linea 5: Validamos el role */
  /* Linea 6: Redirigimos al listado de clientes */
