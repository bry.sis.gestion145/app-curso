import { Component, OnInit } from '@angular/core';
/* Linea 2 */
import { Usuario } from './usuario';
/* Linea 5 */
import swal from 'sweetalert2';
/* Linea 7 */
import { AuthService } from './auth.service';
import { Route } from '@angular/router';
import { Router } from '@angular/router';
/* Linea 11 */
import { NgZone } from "@angular/core";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {

  /* Linea 1 */
  titulo: string = "Sing In!";

  /* Linea 3 */
  usuario: Usuario;

  /* Linea 8 */
  constructor(private auth: AuthService,
              private router: Router,
              /* Linea 12 */
              private zone: NgZone) {
      /* Linea 4 */
      this.usuario = new Usuario();
  }

  ngOnInit(): void {
    if(this.auth.isAuthenticated()){
      swal.fire('Login', `Hola ${this.auth.usuario.username}, ya estas autenticado!`, 'info');
      this.router.navigate(['/clientes']);
    }
    console.log(this.auth.isAuthenticated());
  }

  /* Linea 6 */
  login(): void {
      console.log(this.usuario);
      if(this.usuario.username == null || this.usuario.password == null || this.usuario.username == "" || this.usuario.username == ""){
         swal.fire('Error al Loguearse', 'Username o password vacios!', 'error');
         return;
      }
      /* Linea 9 */
      this.auth.login(this.usuario).subscribe(response => {
        // console.log(response);
        // let payload = JSON.parse(atob(response.access_token.split(".")[1]));
        // console.log(payload);

        this.auth.guardarUsuario(response.access_token);
        this.auth.guardarToken(response.access_token);
        let usuario = this.auth.usuario;
        this.router.navigate(['/clientes']);
        swal.fire('Login', `Hola ${usuario.username}, has iniciado sesion con exito!`, 'success');
      }, err => {
          if(err.status == 400){
            swal.fire('Error Login', `Usuario o contraseña incorrectas`, 'error');
            //this.reloadPage();
          }
      });
  }

 /* Linea 13 */
  reloadPage() {
        this.zone.runOutsideAngular(() => {
            location.reload();
        });
    }

}

/* Tarea 204 */
  /* Linea 1: Crear los atributos de LoginComponent */

/* Tarea 204 */
  /* Linea 2: Importar la clase Usuario */
  /* Linea 3: Crear atributo de tipo Usuario */
  /* Linea 4: En el constructor inicializamos el atributo usuario */
  /* Linea 5: Importamos el sweetalert */
  /* Linea 6: Crear metodo login() */

/* Tarea 214 */
  /* Linea 7: Importar el servicio AuthService, Route y Router */
  /* Linea 8: Inyectamos el servicio AuthService, Route y Router */
  /* Linea 9: Dentro del metodo login llamamos el metodo login del auth service y lo suscribimos */
  /* Linea 10: Manejamos el error al iniciar sesion */
  /* Linea 11: Importar libreria para recargar la pagina */
  /* Linea 12: Inyectar el objeto para recargar pagina */
  /* Linea 13: Implementar metodo para recargar pagina */
  /* Linea 14: LLamamos el metodo que confirma la autenticacion en el OnInit() */
