import { NgModule } from '@angular/core';
/* Linea 1 */
import { RouterModule, Routes } from '@angular/router';
/* Linea 2 */
import { ClientesComponent } from './clientes/clientes.component';
import { DirectivaComponent } from './directiva/directiva.component';
/* Linea 5 */
import { FormComponent } from './clientes/form.component';
/* Linea 9 */
// import { DetalleComponent } from './clientes/detalle/detalle.component';
/* Linea 11 */
import { LoginComponent } from './usuarios/login.component';
/* Linea 13 */
import { AuthGuard } from './usuarios/guards/auth.guard';
/* Linea 15 */
import { RoleGuard } from './usuarios/guards/role.guard';
/* Linea 17 */
import { DetalleFacturaComponent } from './facturas/detalle-factura.component';
/* Linea 19 */
import { FacturasComponent } from './facturas/facturas.component';

/* Linea 3 */
const routes: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {path: 'clientes', component: ClientesComponent },
  {path: 'directivas', component: DirectivaComponent },
  /* Linea 6 */ /* Linea 14 */ /* Linea 16 */
  {path: 'clientes/form', component: FormComponent, canActivate:[AuthGuard, RoleGuard], data: {role: 'ROLE_ADMIN'} },
  /* Linea 7 */ /* Linea 14 */ /* Linea 16 */
  {path: 'clientes/form/:id', component: FormComponent, canActivate:[AuthGuard, RoleGuard], data: {role: 'ROLE_ADMIN'} },
  /* Linea 8 */
  {path: 'clientes/page/:page', component: ClientesComponent },
  /* Linea 10 */
  // {path: 'clientes/ver/:id', component: DetalleComponent }
  /* Linea 12 */
  {path: 'login', component: LoginComponent },
  /* Linea 18 */
  {path: 'facturas/:id', component: DetalleFacturaComponent, canActivate: [AuthGuard, RoleGuard], data: { role: 'ROLE_USER' } },
  /* Linea 19 */
  {path: 'facturas/form/:clienteId', component: FacturasComponent, canActivate: [AuthGuard, RoleGuard], data: { role: 'ROLE_ADMIN' }  }
];

@NgModule({
  /* Linea 4 */
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

/* Tarea 32 */
  /* Linea 1: Importar el router module y el objeto routes */
  /* Linea 2: Importar los componentes que se van a enrutar */
  /* Linea 3: Crear una constante con las rutas de nuestra aplicacion */
  /* Linea 4: Registrar las rutas */

/* Tarea 67 */
  /* Linea 5: Importar el formulario */
  /* Linea 6: Crear la ruta del formulario */

/* Tarea 75 */
  /* Linea 7: Agregar la ruta al FormComponent */

/* Tarea 107 */
  /* Linea 8: Agregar la ruta del paginador */

/* Tarea 130 */
  /* Linea 9: Importar el componente detalle */
  /* Linea 10: Agregar la ruta del componente detalle */

/* Tarea 203 */
  /* Linea 11: Importar el componente login */
  /* Linea 12: Agregar la ruta del componente login */

/* Tarea 224 */
  /* Linea 13: Importamos AuthGuard */
  /* Linea 14: Registramos el guard auth en las rutas */

/* Tarea 227 */
  /* Linea 15: Importamos RoleGuard */
  /* Linea 16: Registramos el guard role en las rutas y pasamos la data */

/* Tarea 225 */
  /* Linea 17: Importamos el detalle de la factura */
  /* Linea 18: Agregarmos la ruta del detalle de la factura */

/* Tarea 264 */
  /* Linea 19: Importamos el form de la factura Agregamos la ruta del formulario de la factura */
