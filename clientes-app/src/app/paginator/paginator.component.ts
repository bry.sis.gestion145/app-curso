import { Component, OnInit } from '@angular/core';
/* Linea 1 */
import { Input } from '@angular/core';
/* Linea 8 */
import { OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'paginator-nav',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.less']
})
/* Linea 9 */
export class PaginatorComponent implements OnInit, OnChanges {

  /* Linea 2 */
  @Input() paginador: any;
  /* Linea 3 */
  paginas: number[] = [];
  /* Linea 5 */
  desde: number = 0;
  hasta: number = 0;

  constructor() {}

  ngOnInit(): void {
    /* Linea 12 */
    this.initPaginator();
  }

  /* Linea 10 */
  ngOnChanges(changes: SimpleChanges): void {
    /* Linea 13 */
    let paginadorActualizado = changes['paginador'];
    if(paginadorActualizado.previousValue){
      this.initPaginator();
    }
  }

  /* Linea 11 */
  private initPaginator(): void {
    /* Linea 6 */
    this.desde = Math.min(Math.max(1, this.paginador.number-4), this.paginador.totalPages - 5);
    this.hasta = Math.max(Math.min(this.paginador.totalPages, this.paginador.number+4), 6);
    /* Linea 7 */
    if(this.paginador.totalPages > 5) {
      this.paginas = new Array(this.hasta - this.desde + 1).fill(0).map((_valor, indice) => indice + this.desde);
    } else {
      /* Linea 4 */
      this.paginas = new Array(this.paginador.totalPages).fill(0).map((_valor, indice) => indice + 1);
    }
  }

}

/* Tarea 112 */
  /* Linea 1: Importamos la libreria para entradas @Input */
  /* Linea 2: Creamos una entrada @Input para recibir el contenido del paginador desde clientes component */
  /* Linea 3: Crear un atributo paginas */
  /* Linea 4: Asignamos el atibuto paginas en el constructor y trabajamos con el arreglo */

/* Tarea 114: Generando rangos en el paginador */
  /* Linea 5: Creacion de los atributos desde y hasta para los rangos del paginador */
  /* Linea 6: Hacemos el calculo de los rangos */
  /* Linea 7: Utilizamos una condicion para que trabaje con rangos si el numero de paginas es > 5 */
  /* Linea 8: Importamos el evento onChanges() y el parametro SimpleChanges */
  /* Linea 9: Implementamos la interface OnChanges */
  /* Linea 10: Utilizamos el metodo onChanges() para que se actualize el componente paginator.component
     cada vez que nos movamos por los rangos */
  /* Linea 10: En el metodo onChange() creamos el parametro SimpleChanges */

/* Tarea 115: Optimizando el paginador */
  /* Linea 11: Creamos un metodo initPaginator() con el calculo de los rangos y la condicion de los mismos */
  /* Linea 12: Llamamos el metodo initPaginator() en el metodo OnInit() */
  /* Linea 13: Llamamos el metodo initPaginator() en el metodo OnChange() */
