import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.less']
})
export class FooterComponent implements OnInit {

  /* Linea 1 */
  autor: any = { nombre: 'Bryan', apellido: 'Gonzalez' };

  constructor() { }

  ngOnInit(): void {
  }

}

/* Tarea 7 */
  /* Linea 1: Crear el objeto autor de tipo generico */
