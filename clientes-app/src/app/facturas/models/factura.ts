/* Linea 1 */
import { ItemFactura } from './item-factura';
/* Linea 2 */
import { Cliente } from '../../clientes/cliente';

export class Factura {
  /* Linea 3 */
  id: number;
  descripcion: string;
  observacion: string;
  items: ItemFactura[] = [];
  cliente: Cliente;
  total: number;
  createAt: string;

  /* Linea 4 */
  calcularGranTotal(): number {
    this.total = 0;
    this.items.forEach((item: ItemFactura) => {
      this.total += item.calcularImporte();
    });
    return this.total;
  }

}

/* Tarea 251 */
  /* Linea 1: Importar ItemFactura */
  /* Linea 2: Importar el cliente */
  /* Linea 3: Crear los atributos de la factura */
  /* LInea 4: Calcular metodo calcularGranTotal() */
