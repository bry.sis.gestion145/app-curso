/* Linea 1 */
import { Producto } from './producto';

export class ItemFactura {
  producto: Producto;
  cantidad: number = 1;
  importe: number;

  public calcularImporte(): number {
    return this.cantidad * this.producto.precio;
  }

}

/* Tarea 250 */
  /* Linea 1: Importar el producto */
