import { Component, OnInit } from '@angular/core';
/* Linea 1 */
import { Factura } from './models/factura';
import { FacturaService } from './services/factura.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detalle-factura',
  templateUrl: './detalle-factura.component.html',
  styleUrls: ['./detalle-factura.component.less']
})
export class DetalleFacturaComponent implements OnInit {

  /* Linea 3 */
  factura: Factura;
  titulo: string = 'Factura';


  /* Linea 2 */
  constructor(private facturaService: FacturaService,
              private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    /* Linea 4 */
    this.activatedRoute.paramMap.subscribe(params => {
      let id = +params.get('id');
      this.facturaService.getFactura(id).subscribe(factura => {
          this.factura = factura
      });
    });
  }

}

/* Tarea 258 */
  /* Linea 1: Importamos la clase factura, el servicio FacturasService y el ActivatedRoute */
  /* Linea 2: Inyectamos FacturasService y el ActivatedRoute */
  /* Linea 3: Creamos el atributo factura y el resto de los atributos */
  /* Linea 4: Obtenemos el id de la factura que estamos recibiendo y obtenemos la factura */
