import { Injectable } from '@angular/core';
/* Linea 1A */
import { HttpClient } from '@angular/common/http';
/* Linea 2 */
import { Observable } from 'rxjs';
/* Linea 3 */
import { Factura } from '../models/factura';
import { Producto } from '../models/producto';

@Injectable({
  providedIn: 'root'
})
export class FacturaService {

  /* Linea 4 */
  private urlEndPoint: string = 'http://localhost:8080/api/facturas';

  /* Linea 1B */
  constructor(private http: HttpClient) { }

  /* Linea 5 */
  getFactura(id:number): Observable<Factura>{
    return this.http.get<Factura>(`${this.urlEndPoint}/${id}`);
  }

  /* Linea 6 */
  delete(id: number): Observable<void> {
    return this.http.delete<void>(`${this.urlEndPoint}/${id}`);
  }

  /* Linea 7 */
  filtrarProductos(term: string): Observable<Producto[]> {
    return this.http.get<Producto[]>(`${this.urlEndPoint}/filtrar-productos/${term}`);
  }

  /* Linea 7 */
  create(factura: Factura): Observable<Factura> {
    return this.http.post<Factura>(this.urlEndPoint, factura);
  }

}

/* Tarea 257 */
  /* Linea 1A: Importar el http client */
  /* Linea 1B: Inyectar el http client */
  /* Linea 2: Importar observable */
  /* Linea 3: Importamos la factura y el producto */
  /* Linea 4: Crear un urlEndPoint del controlador */
  /* Linea 5: Creamos el metodo getFactura() */

/* Tarea 260 */
  /* Linea 6: Crear el metodo para borrar factura */
  /* Linea 7: Crear los metodos filtrarProductos() y crear() */
