import { Component, OnInit } from '@angular/core';
/* Linea 1 */
import { Factura } from '../facturas/models/factura';
import { FacturaService } from '../facturas/services/factura.service';
import { ClienteService } from '../clientes/cliente.service';
import { ActivatedRoute, Router } from '@angular/router';
/* Linea 1 */
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, flatMap } from 'rxjs/operators';
import { Producto } from './models/producto';
import { ItemFactura } from './models/item-factura';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import swal from 'sweetalert2';

@Component({
  selector: 'app-facturas',
  templateUrl: './facturas.component.html',
  styleUrls: ['./facturas.component.less']
})
export class FacturasComponent implements OnInit {

  /* Linea 3 */
  titulo: string = "Nueva Factura";
  factura: Factura = new Factura();
  autocompleteControl = new FormControl();
  productosFiltrados: Observable<Producto[]>;

  /* Linea 2 */
  constructor(private facturaService: FacturaService,
              private clienteService: ClienteService,
              private activatedRoute: ActivatedRoute,
              private router: Router) { }

  ngOnInit(): void {
    /* Linea 4 */
    this.activatedRoute.paramMap.subscribe(params => {
      let clienteId = +params.get('clienteId');
      this.clienteService.getCliente(clienteId).subscribe(cliente => {
        this.factura.cliente = cliente;
      });
    });
    /* Linea 6 */
    this.productosFiltrados = this.autocompleteControl.valueChanges
      .pipe(
        map(value => typeof value === 'string' ? value : value.nombre),
        flatMap(value => value ? this._filter(value) : [])
      );
  }

  /* Linea 5 */
  private _filter(value: string): Observable<Producto[]> {
      const filterValue = value.toLowerCase();

      return this.facturaService.filtrarProductos(filterValue);
  }

  /* Linea 7 */
  mostrarNombre(producto?: Producto): string | undefined {
    return producto ? producto.nombre : undefined;
  }

  /* Linea 8 */
  seleccionarProducto(event: MatAutocompleteSelectedEvent): void {
  let producto = event.option.value as Producto;
  console.log(producto);

  if (this.existeItem(producto.id)) {
    this.incrementaCantidad(producto.id);
  } else {
    let nuevoItem = new ItemFactura();
    nuevoItem.producto = producto;
    this.factura.items.push(nuevoItem);
  }

  this.autocompleteControl.setValue('');
  event.option.focus();
  event.option.deselect();

}

/* Linea 9 */
actualizarCantidad(id: number, event: any): void {
  let cantidad: number = event.target.value as number;

  if (cantidad == 0) {
    return this.eliminarItemFactura(id);
  }

  this.factura.items = this.factura.items.map((item: ItemFactura) => {
    if (id === item.producto.id) {
      item.cantidad = cantidad;
    }
    return item;
  });
}

/* Linea 10 */
existeItem(id: number): boolean {
  let existe = false;
  this.factura.items.forEach((item: ItemFactura) => {
    if (id === item.producto.id) {
      existe = true;
    }
  });
  return existe;
}

/* Linea 11 */
incrementaCantidad(id: number): void {
  this.factura.items = this.factura.items.map((item: ItemFactura) => {
    if (id === item.producto.id) {
      ++item.cantidad;
    }
    return item;
  });
}

/* Linea 12 */
eliminarItemFactura(id: number): void {
  this.factura.items = this.factura.items.filter((item: ItemFactura) => id !== item.producto.id);
}

/* Linea 13 */
create(facturaForm): void {
  console.log(this.factura);
  if (this.factura.items.length == 0) {
    this.autocompleteControl.setErrors({ 'invalid': true });
  }

  if (facturaForm.form.valid && this.factura.items.length > 0) {
    this.facturaService.create(this.factura).subscribe(factura => {
      if(this.factura.items.length == 0){
        this.autocompleteControl.setErrors({'invalid':true});
      }
      if(facturaForm.form.valid && this.factura.items.length > 0){
        swal.fire(this.titulo, `Factura ${factura.descripcion} creada con éxito!`, 'success');
        /* Linea 14 */
        this.router.navigate(['/facturas', factura.id]);
        // this.router.navigate(['/clientes']);
      }

    });
  }
}

}

/* Tarea  265 */
  /* Linea 1: Importar Factura, FacturaService, ClienteService y ActivatedRoute, Router y demas imports */
  /* Linea 2: Inyectar FacturaService, ClienteService, ActivatedRoute y Router */
  /* Linea 3: Crear los atributos del formulario */
  /* Linea 4: Obtenemos el id del cliente */
  /* Linea 5: Crear metodo filtrar productos */
  /* Linea 6: Filtrar los productos en el onInit */
  /* Linea 7: Crear metodo mostrar nombre de la factura */
  /* Linea 8: Crear metodo seleccionarProducto() */
  /* Linea 9: Crear metodo actualizarCantidad() */
  /* Linea 10: Crear metodo existeItem() */
  /* Linea 11: Crear metodo incrementaCantidad() */
  /* Linea 12: Crear metodo eliminarItemFactura() */
  /* Linea 13: Crear metodo create() */

/* Tarea 216 */
  /* Linea 14: Dejamos de redirigir a los clientes para redirigir al detalle de la factura */
