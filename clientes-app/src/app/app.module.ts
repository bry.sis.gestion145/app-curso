import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { DirectivaComponent } from './directiva/directiva.component';
import { ClientesComponent } from './clientes/clientes.component';
/* Linea 3 */
import { HttpClientModule } from '@angular/common/http';

/* Linea 1 */
import { ClienteService } from './clientes/cliente.service';
import { FormComponent } from './clientes/form.component';

/* Linea 5 */
import { FormsModule } from '@angular/forms';
/* Linea 7 */
import localeES from '@angular/common/locales/es';
import { registerLocaleData } from '@angular/common';
/* Linea 9 */
import { LOCALE_ID } from '@angular/core';
import { PaginatorComponent } from './paginator/paginator.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
/* Linea 11 */
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { DetalleComponent } from './clientes/detalle/detalle.component';
import { LoginComponent } from './usuarios/login.component';
/* Linea 13 */
import { TokenInterceptor } from './usuarios/interceptors/token.interceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
/* Linea 15 */
import { AuthInterceptor } from './usuarios/interceptors/auth.interceptor';
import { DetalleFacturaComponent } from './facturas/detalle-factura.component';
import { FacturasComponent } from './facturas/facturas.component';

/* Imports para facturas.component.html Linea x */
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ReactiveFormsModule } from '@angular/forms';

/* Linea 8 */
registerLocaleData(localeES, 'es');

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    DirectivaComponent,
    ClientesComponent,
    FormComponent,
    PaginatorComponent,
    DetalleComponent,
    LoginComponent,
    DetalleFacturaComponent,
    FacturasComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    /* Linea 6 */
    FormsModule,
    BrowserAnimationsModule,
    /* Linea 12 */
    MatDatepickerModule,
    MatMomentDateModule,
    /* Linea x */
    ReactiveFormsModule,
    MatAutocompleteModule, MatInputModule, MatFormFieldModule
  ],
  /* Linea 2 */ /* Linea 10 */ /* Linea 14 */ /* Linea 16 */
  providers: [ClienteService,
             {provide: LOCALE_ID, useValue: 'es'},
             {provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true},
             {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }

/* Tarea 29 */
  /* Linea 1: Importar el servicio cliente */
  /* Linea 2: Registrar el servicio cliente para uso global */

/* Tarea 57 */
    /* Linea 3: Importar el modulo HttpClientModule */
    /* Linea 4: Registrar el HttpClientModule en los imports */

/* Tarea 64 */
    /* Linea 5: Importar el FormsModule */
    /* Linea 6: Registrar el FormsModule en Imports */

/* Tarea 96 */
    /* Linea 7: Importar la libreria localeES y la funcion registerLocaleData */
    /* Linea 8: Registrar el locale */

/* Tarea 98 */
    /* Linea  9: Importar la variable LOCALE_ID */
    /* Linea 10: Registrar la variable LOCALE_ID en providers */

/* Tarea 120 */
    /* Linea 11: Importar los modulos MatDatepickerModule, MatMomentDateModule */
    /* Linea 12: Registrar los modulos importados */

/* Tarea 231 */
    /* Linea 13: Importamos el interceptor Token y la constante HTTP_INTERCEPTORS */
    /* Linea 14: Registrar el interceptor Token */

/* Tarea 231 */
    /* Linea 15: Importamos el interceptor Auth  */
    /* Linea 16: Registrar el interceptor Auth */
