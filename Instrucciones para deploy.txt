 1. Nos ubicamos sobre el directoria de nuestro proyecto.

 2. Generar el .jar de nuestro proyecto de spring boot
    Nos vamos a la linea de comandos sobre el directorio del proyecto y escribimos el comando: .\mvnw.cmd package
    y esperamos que se compile nuestro .jar del proyecto de spring boot.

 3. Generar el JavaScript de nuestro proyecto de angular
    Nos vamos a la linea de comandos sobre el directorio del proyecto y escribimos el comando: ng build --prod
    y esperamos que se transpile el JavaScrip de nuestro proyecto de angular

 4. Configurar el proyecto traspilado de JavaScript.
    4.1 Dentro de la carpeta Dist creamos un nuevo archivo ".htaccess".
    4.2 Dentro de nuestro archivo creado ".htaccess" copiamos el siguiente codigo.
       RewriteEngine On
       # If an existing asset or directory is requested go to it as it is
       RewriteCond %{DOCUMENT_ROOT}%{REQUEST_URI} -f [OR]
       RewriteCond %{DOCUMENT_ROOT}%{REQUEST_URI} -d
       RewriteRule ^ - [L]

       # If the requested resource doesn't exist, use index.html
       RewriteRule ^ /index.html

    4.3 Dentro del codigo del archivo ".htaccess" hacemos los siguientes cambios.
       RewriteEngine On
       # If an existing asset or directory is requested go to it as it is
       RewriteCond %{REQUEST_FILENAME} -f [OR]
       RewriteCond %{REQUEST_FILENAME} -d
       RewriteRule ^ - [L]

       # If the requested resource doesn't exist, use index.html
       RewriteRule ^ /clientes-app/index.html

 5. Nos vamos al archivo index.html y en la etiqueta <base href="/"> le ponemos el link del directorio base:
   <base href="/clientes-app"> 

*******************************************************************************************************************************************

 6. Desplegar el Front-End en un servidor apache : (Video 194) https://www.udemy.com/course/angular-spring/learn/lecture/13489736#questions

 7. Desplegar el Front-End con NodeJs y Express  : (Video 195) https://www.udemy.com/course/angular-spring/learn/lecture/13489774#questions
*******************************************************************************************************************************************
DEPLOY EN FIREBASE Y HEROKU
 8. Creando un app en heroku                     : (Video 197) https://www.udemy.com/course/angular-spring/learn/lecture/13545994#questions

 9. Deploy Backend en heroku                     : (Video 198) https://www.udemy.com/course/angular-spring/learn/lecture/13545968#questions

10. Deploy Front-End en Firebase                 : (Video 199) https://www.udemy.com/course/angular-spring/learn/lecture/13545978#questions
*******************************************************************************************************************************************
   