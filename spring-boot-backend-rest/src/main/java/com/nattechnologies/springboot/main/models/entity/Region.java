package com.nattechnologies.springboot.main.models.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/* Linea 2 */
@Entity
@Table(name="regiones")
public class Region implements Serializable {

	
	@Id /* Linea 3 */
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private String nombre;
	
	/* Linea 3 */
	public Long getId() {
		return id;
	}

	/* Linea 3 */
	public void setId(Long id) {
		this.id = id;
	}

	/* Linea 3 */
	public String getNombre() {
		return nombre;
	}

	/* Linea 3 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	/* Linea 1 */
	private static final long serialVersionUID = 1L;

}

/* Tarea 158 */
  /* Linea 1: Implementamos de la entidad Serializable y copiamos el atributo ID de serializable */
  /* Linea 2: Anotamos la clase para indicar que es un entity o tabla en la base de datos */
  /* Linea 3: Implementamos los atributos de la clase Region con sus respectivas anotaciones, ademas creamos
              los atributos get y set de los campos */
