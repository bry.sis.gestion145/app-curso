package com.nattechnologies.springboot.main.controllers;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.nattechnologies.springboot.main.models.entity.Cliente;
import com.nattechnologies.springboot.main.models.entity.Region;
import com.nattechnologies.springboot.main.models.services.IClienteService;
import com.nattechnologies.springboot.main.models.services.IUploadFileService;

// @CrossOrigin(origins = {"http://localhost:4200/", "*"}) /* Linea 31 */
@CrossOrigin(origins = {"http://localhost:4200/"})  /* Linea 6 */
@RestController                                          /* Linea 1 */
@RequestMapping("/api")                                  /* Linea 1 */
public class ClienteRestController {
	/* Linea 4 */
	@Autowired
	private IClienteService clienteService;
	
	/* Linea 24 */
	@Autowired IUploadFileService uploadService;
	
	/* Linea 22 */
	private final Logger log = LoggerFactory.getLogger(ClienteRestController.class);
    
	/* Linea 3 */
	@GetMapping("/clientes")
	public List<Cliente> index() { /* Linea 2 */
		/* Linea 5 */
		return clienteService.findAll();
	}
	
	/* Linea 7 */
	@GetMapping("/clientes/page/{page}")
	public Page<Cliente> index(@PathVariable Integer page) { 
		Pageable pageable = PageRequest.of(page, 4);
		return clienteService.findAll(pageable);
	}
	
	/* Linea 11 */  
	@Secured({"ROLE_USER", "ROLE_ADMIN"}) /* Linea 30 */
    @GetMapping("/clientes/{id}")
	public ResponseEntity<?> show(@PathVariable Long id) {
    	Cliente cliente = null;
    	Map<String, Object> response = new HashMap<>();
    	/* Manejo de errores de la base de datos */
    	try {
    		cliente = clienteService.findById(id);
    	} catch (DataAccessException e) {
    		response.put("mensaje", "Error al realizar la consulta en la base de datos!!!");
    		response.put("Error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
    		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
    	/* Manejo de error si el cliente es nulo */
    	if(cliente == null) {
    		response.put("mensaje", "El cliente ID: ".concat(id.toString()).concat(" no existe en la base de datos!"));
    		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
    	}
		return new ResponseEntity<Cliente>(cliente, HttpStatus.OK);
	}
	
	/* Linea 7 *//*  
    @GetMapping("/clientes/{id}")
	public Cliente show(@PathVariable Long id) {
		return clienteService.findById(id);
	}
	*/
	
    /* Linea 15 */
	@Secured("ROLE_ADMIN") /* Linea 30 */
	@PostMapping("/clientes")
	public ResponseEntity<?> create(@Valid @RequestBody Cliente cliente, BindingResult result) {
		Cliente clienteNew = null;
		Map<String, Object> response = new HashMap<>();
		
		if(result.hasErrors()) {
			/* Convertir errores a String Forma 1*//*
			List<String> errors = new ArrayList<>();
			for(FieldError err: result.getFieldErrors()) {
				errors.add("El campo: '" + err.getField() + "' " +err.getDefaultMessage());
			}*/
			
			/* Convertir errores a String Forma 2*/
			List<String> errors = result.getFieldErrors()
					              .stream()
					              .map(err -> {
					            	  return "El campo: '" + err.getField() + "' " +err.getDefaultMessage();
					              })
					              .collect(Collectors.toList());
			
			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		
		try {
			clienteNew = clienteService.save(cliente);
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar el insert en la base de datos!!!");
			response.put("Error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
    		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("mensaje", "El cliente ha sido creado con exito!");
		response.put("cliente", clienteNew);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
    
    /* Linea 12 */ /*
	@PostMapping("/clientes")
	public ResponseEntity<?> create(@RequestBody Cliente cliente) {
		Cliente clienteNew = null;
		Map<String, Object> response = new HashMap<>();
		try {
			clienteNew = clienteService.save(cliente);
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar el insert en la base de datos!!!");
			response.put("Error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
    		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("mensaje", "El cliente ha sido creado con exito!");
		response.put("cliente", clienteNew);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	} */
    
	/* Linea 8 *//*
	@PostMapping("/clientes")
	@ResponseStatus(HttpStatus.CREATED)
	public Cliente create(@RequestBody Cliente cliente) {
		return clienteService.save(cliente);
	}
	*/
	
	/* Linea 16 */
	@Secured("ROLE_ADMIN") /* Linea 30 */
	@PutMapping("/clientes/{id}")
	public ResponseEntity<?> update(@Valid @RequestBody Cliente cliente, BindingResult result, @PathVariable Long id) {
		Cliente clienteActual = clienteService.findById(id);
		Cliente clienteUpdated = null;
		Map<String, Object> response = new HashMap<>();
		
		if(result.hasErrors()) {
			/* Convertir errores a String Forma 1*//*
			List<String> errors = new ArrayList<>();
			for(FieldError err: result.getFieldErrors()) {
				errors.add("El campo: '" + err.getField() + "' " +err.getDefaultMessage());
			}*/
			
			/* Convertir errores a String Forma 2*/
			List<String> errors = result.getFieldErrors()
					              .stream()
					              .map(err -> {
					            	  return "El campo: '" + err.getField() + "' " +err.getDefaultMessage();
					              })
					              .collect(Collectors.toList());
			
			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		
		if(clienteActual == null) {
    		response.put("mensaje", "Error: no se pudo editar, el cliente ID: ".concat(id.toString()).concat(", no existe en la base de datos!"));
    		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
    	}
		try {
			clienteActual.setApellido(cliente.getApellido());
			clienteActual.setNombre(cliente.getNombre());
			clienteActual.setEmail(cliente.getEmail());
			clienteActual.setCreateAt(cliente.getCreateAt());
			/* Linea 29 */
			clienteActual.setRegion(cliente.getRegion());
			/* ******** */
			clienteUpdated = clienteService.save(clienteActual);
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al actualizar el cliente en la base de datos!!!");
    		response.put("Error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
    		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("mensaje", "El cliente ha sido actualizado con exito!");
		response.put("cliente", clienteUpdated);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	
	/* Linea 13 *//*
	@PutMapping("/clientes/{id}")
	public ResponseEntity<?> update(@RequestBody Cliente cliente, @PathVariable Long id) {
		Cliente clienteActual = clienteService.findById(id);
		Cliente clienteUpdated = null;
		Map<String, Object> response = new HashMap<>();
		if(clienteActual == null) {
    		response.put("mensaje", "Error: no se pudo editar, el cliente ID: ".concat(id.toString()).concat(", no existe en la base de datos!"));
    		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
    	}
		try {
			clienteActual.setApellido(cliente.getApellido());
			clienteActual.setNombre(cliente.getNombre());
			clienteActual.setEmail(cliente.getEmail());
			clienteActual.setCreateAt(cliente.getCreateAt());
			clienteUpdated = clienteService.save(clienteActual);
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al actualizar el cliente en la base de datos!!!");
    		response.put("Error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
    		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("mensaje", "El cliente ha sido actualizado con exito!");
		response.put("cliente", clienteUpdated);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}*/
	
	/* Linea 9 *//*
	@PutMapping("/clientes/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public Cliente update(@RequestBody Cliente cliente, @PathVariable Long id) {
		Cliente clienteActual = clienteService.findById(id);
		clienteActual.setApellido(cliente.getApellido());
		clienteActual.setNombre(cliente.getNombre());
		clienteActual.setEmail(cliente.getEmail());
		return clienteService.save(clienteActual);
	}*/
	
	/* Linea 14 */
	@Secured("ROLE_ADMIN")
	@DeleteMapping("/clientes/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {
		Map<String, Object> response = new HashMap<>();
		try {
			Cliente cliente = clienteService.findById(id);
			String nombreFotoAnterior = cliente.getFoto();
		  /*if(nombreFotoAnterior != null && nombreFotoAnterior.length() > 0) {
			   Path rutaFotoAnterior = Paths.get("uploads").resolve(nombreFotoAnterior).toAbsolutePath();
			   File archivoFotoAnterior = rutaFotoAnterior.toFile();
			   if(archivoFotoAnterior.exists() && archivoFotoAnterior.canRead()) {
				   archivoFotoAnterior.delete();
			   }
			}*/
			/* Linea 25 */
			uploadService.eliminar(nombreFotoAnterior);
			
		    clienteService.delete(id);
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al eliminar el cliente en la base de datos!!!");
    		response.put("Error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
    		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}    
		response.put("mensaje", "El cliente ha sido eliminado con exito!");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	
	/* Linea 10 *//*
	@DeleteMapping("/clientes/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Long id) {
		clienteService.delete(id);
	}*/
	
	/* Linea 20 */
	@Secured({"ROLE_USER", "ROLE_ADMIN"}) /* Linea 30 */
	@PostMapping("/clientes/upload")
	public ResponseEntity<?> upload(@RequestParam("archivo") MultipartFile archivo, @RequestParam("id") Long id) {
		Map<String, Object> response = new HashMap<>();
		
		Cliente cliente = clienteService.findById(id);
		
		if(!archivo.isEmpty()) {
		 // String nombreArchivo = UUID.randomUUID().toString() + "__" + archivo.getOriginalFilename().replace(" ", "");
		 // Path rutaArchivo = Paths.get("uploads").resolve(nombreArchivo).toAbsolutePath();
			// Linea 23 
	     // log.info(rutaArchivo.toString());
			String nombreArchivo = null;
			try {
				// Files.copy(archivo.getInputStream(), rutaArchivo);
				/* Linea 26 */
				nombreArchivo = uploadService.guardar(archivo);
			} catch (IOException e) {
				response.put("mensaje", "Error al subir la imagen del cliente");
	    		response.put("Error", e.getMessage().concat(": ").concat(e.getCause().getMessage()));
	    		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			} 
			
			String nombreFotoAnterior = cliente.getFoto();
			uploadService.eliminar(nombreFotoAnterior);
		  /*if(nombreFotoAnterior != null && nombreFotoAnterior.length() > 0) {
			   Path rutaFotoAnterior = Paths.get("uploads").resolve(nombreFotoAnterior).toAbsolutePath();
			   File archivoFotoAnterior = rutaFotoAnterior.toFile();
			   if(archivoFotoAnterior.exists() && archivoFotoAnterior.canRead()) {
				   archivoFotoAnterior.delete();
			   }
			}*/
			
			cliente.setFoto(nombreArchivo);
			clienteService.save(cliente);
			
			response.put("cliente", cliente);
			response.put("mensaje", "Has subido correctamente la imagen: "+ nombreArchivo);
			
		}
		
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	
	/* Linea 21 */
	@GetMapping("/uploads/img/{nombreFoto:.+}")
	public ResponseEntity<Resource> verFoto(@PathVariable String nombreFoto){
	/*	Path rutaArchivo = Paths.get("uploads").resolve(nombreFoto).toAbsolutePath();
		// Linea 23 
		log.info(rutaArchivo.toString());*/
		
		Resource recurso = null;
	 /* try {
			recurso = new UrlResource(rutaArchivo.toUri());
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		if(!recurso.exists() && !recurso.isReadable()) {
			rutaArchivo = Paths.get("src/main/resources/static/images").resolve("no-usuario.png").toAbsolutePath();
			try {
				recurso = new UrlResource(rutaArchivo.toUri());
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
			log.error("Error no se pudo cargar la imagen: " + nombreFoto);
		} */
		try {
			/* Linea 27 */
			recurso = uploadService.cargar(nombreFoto);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		HttpHeaders cabecera = new HttpHeaders();
		cabecera.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename\"" + recurso.getFilename() + "\"");
		
		return new ResponseEntity<Resource>(recurso, cabecera, HttpStatus.OK);
	}
	
	/* Linea 28 */
	@Secured("ROLE_ADMIN")
	@GetMapping("/clientes/regiones")
	public List<Region> listarRegiones(){
		return clienteService.findAllRegiones();
	}
	
}

/* Tarea 53 */
  /* Linea 1: Decorar la clase con la anotacion RestController */
  /*        : Mapear el rest controller con su endpoint respectivo, anotar con RequestMapping */
  /* Linea 2: Crear metodo index para listar los clientes */
  /* Linea 3: Anotar el metodo index con la anotacion GetMepping */
  /* Linea 4: Inyectar la clase IClienteService */
  /* Linea 5: Crear el retorno del metodo index() */

/* Tarea 56 */
  /* Linea 6: Anotar el controlador con CrossOrigin para configurar CORS general que no tiene que ver con JWT */

/* Tarea 61 */
  /* Linea  7: Crear accion buscar cliente por id */
  /* Linea  8: Crear accion crear cliente */
  /* Linea  9: Crear accion actualizar cliente */
  /* Linea 10: Crear accion borrar cliente */

/* Tarea 86 */
  /* Linea 11: Implementar manejo de errores en el metodo Show   */
  /* Linea 12: Implementar manejo de errores en el metodo Create */
  /* Linea 13: Implementar manejo de errores en el metodo Update */
  /* Linea 14: Implementar manejo de errores en el metodo Delete */

/* Tarea 90 */
  /* Linea 15: Validar el objeto cliente en el metodo create() con @Valid y Binding Result */
  /* Linea 16: Validar el objeto cliente en el metodo update() con @Valid y Binding Result */

/* Tarea 104 */
  /* Linea 17: Implementar la accion de nuestro metodo findAll() Pageable */

/* Tarea 123 */
  /* Linea 19: Creamos la carpeta 'uploads' para subir imagenes dentro del directorio principal de nuestro proyecto
     esta carpeta es de practica, cuando se sube el proyecto al servidor la carpeta se encuentra en otro directorio
     externo al proyecto y no dentro del mismo */
  /* Linea 20: Implementar el metodo upload */
  /* Linea 21: Implementar el metodo para mostrar la imagen en el navegador */

/* Tarea 126: Crear un log para mostrar la ruta del archivo */
  /* Linea 22: Crear un atributo log */
  /* Linea 23: Mostrar la ruta de archivo usando el log en los metodos upload() y verFoto() */

/* Tarea 155 */
  /* Linea 24:  Inyectamos el service IUploadFileService */
  /* Linea 25:  Modificamos el metodo delete para que haga uso de la clase UploadFileService */
  /* Linea 26:  Modificamos el metodo upload para que haga uso de la clase UploadFileService */
  /* Linea 27:  Modificamos el metodo verFoto para que haga uso de la clase UploadFileService */

/* Tarea 163 */
  /* Linea 28: Implementar el metodo get para listar las regiones */
  /* Linea 29: Modificaciones al metodo Update */

/* Tarea 200 */
  /* Linea 30: Implementar los permisos con la anotacion @Secure */

/* Tarea 250 */
  /* Linea 31: En la notacion CrossOrigin ponemos la ruta que  habiamos puesto en el ResourseServerConfig */







