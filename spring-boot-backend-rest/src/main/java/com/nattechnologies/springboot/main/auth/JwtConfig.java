package com.nattechnologies.springboot.main.auth;

public class JwtConfig {

	/* Linea 1 */
	public static final String LLAVE_SECRETA = "alguna.clave.secreta.12345678";
	
	/* Linea 2 */
	public static final String RSA_PUBLICA = "-----BEGIN PUBLIC KEY-----\r\n"
			+ "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwzk7MfoTmdTPggJ1Mp+c\r\n"
			+ "bq9uPhAYExj6gYaeXzGSAtfEkMrPV7H9aOF0zRMfyqLSTDYX89hJfW40p2OgLXAh\r\n"
			+ "RIDPtOr/opH76A1P6xBIeuVV/h0Yu9mOqIKAIGbwR5o/0Ghpi9mKJyDdxrgLEyY6\r\n"
			+ "764RBjoNyeL16pjH+543cm5vJ0J/NL8HS2CbGpdc0KRtCxwqYzg2mLSCsDnCghzf\r\n"
			+ "RfrA6q3v6lv7jwGttrYDLyPx7jyeOGezopVLapy19cNHAaxcb4DyKT7RQOXwbtot\r\n"
			+ "d+l/cLdPn6Ma1FQr7b2bNBWth5qXOvtjQvoMlgYBkxf8yIWlAn5qoWzhl4dh3FHX\r\n"
			+ "sQIDAQAB\r\n"
			+ "-----END PUBLIC KEY-----";
	
	/* Linea 3 */	
	public static final String RSA_PRIVADA = "-----BEGIN RSA PRIVATE KEY-----\r\n"
			+ "MIIEowIBAAKCAQEAwzk7MfoTmdTPggJ1Mp+cbq9uPhAYExj6gYaeXzGSAtfEkMrP\r\n"
			+ "V7H9aOF0zRMfyqLSTDYX89hJfW40p2OgLXAhRIDPtOr/opH76A1P6xBIeuVV/h0Y\r\n"
			+ "u9mOqIKAIGbwR5o/0Ghpi9mKJyDdxrgLEyY6764RBjoNyeL16pjH+543cm5vJ0J/\r\n"
			+ "NL8HS2CbGpdc0KRtCxwqYzg2mLSCsDnCghzfRfrA6q3v6lv7jwGttrYDLyPx7jye\r\n"
			+ "OGezopVLapy19cNHAaxcb4DyKT7RQOXwbtotd+l/cLdPn6Ma1FQr7b2bNBWth5qX\r\n"
			+ "OvtjQvoMlgYBkxf8yIWlAn5qoWzhl4dh3FHXsQIDAQABAoIBAQCWQtfAKNarYbRy\r\n"
			+ "QggI9fdG7npxBJwzHte5mG8PoCzdSUw/kgqzWglNj7MCIYUnx9kL1drr0f8GryuR\r\n"
			+ "50mjj5nbizXT78jhTXHR82Ue7YKMLTbaB3VBKTv3kbKivfiVdDFH7QXIIaGHanGp\r\n"
			+ "AYFstNFKJ/VskISKbsmPTxcmIWpWBTgccLRzwk6rcSS3t/ZnwzwxUpUpIuMI7DzU\r\n"
			+ "az3thE/tC2EX+2KQR2dMRbH3l6BYHLWZt795+hjAVumnigIoCUnjrrLZjjFuPIoI\r\n"
			+ "y2EzZorQedtYisnchOfhp2NKiskyPqLYVDvmG46IoUDZQQwEh/0Qzt8RKNm84oVC\r\n"
			+ "yxibow7BAoGBAOEa0WOPSrXhgJ/HLZIPuXgHKR8BSt61mGQglTvs7Xdjx9GujTSi\r\n"
			+ "OM5W5oLBgGJt65Knb2aabXM1bOZ1+fvJ4mXr5bSPJHGRFSDRkLmD5m0vvSabduWT\r\n"
			+ "ppa8snipYRKvXOEHa4tpR4lRk2VyBn3rGhXPvI6TQ+V4rHn6O8eAHhb5AoGBAN4E\r\n"
			+ "hUgkUPVEdnSqYXP9/2eWKtjbXg3MNG9klz4aMhAYBKZE7jy+GLv/LKZr/iSV0Cr9\r\n"
			+ "y6ScJcqxil9+GYpZX7W2xf+WQrIie6uBXhC+lmZaKnhba22eOtNCkKiYN0N3pPR6\r\n"
			+ "GTEhCxfP0MOuFyCFyoZNt26mChWkRtD5xAbE89x5AoGAEzpVNGni+dI/SGadD+Lj\r\n"
			+ "9sq1yn4zxP5B/BuEY9ADILWXNePxD9PjgkyN+BFLRB0biDVhIKSEQNkJak4dglJm\r\n"
			+ "UkpqHpgzhrfb1CfScuY/Yog3YfrZlxabnjiUDhZ5wjzUdCxMFWZyw9oB+yJYR50z\r\n"
			+ "ZzWNZ6hQnmNGaEzlmk7pQxECgYA/Ojpa2TzVHLI/d2ZZijR6Y6HAMHkQpN8p+hO4\r\n"
			+ "c+1d9/kbAPZ4Bgs1Pu6QVqEB+khWdgDQ2EdK+lTV7SuI4vsG3Q52Q2yrHnbp2OXZ\r\n"
			+ "Du32KCeAL0jg5vcJwyM/YFoctK4dkcgq3aYzGdPyZlnfwTp0FR3wPQkSML6aNQdG\r\n"
			+ "hkCMwQKBgArelPwRMkGy0Ofal4/t9KPKxrdEnYQcU3aZ75HCwjTU/D5eWHOg+99i\r\n"
			+ "mQA97nKSWGvjYyqW0DE2B3ptV0jB4VhOD9jwHFLCCLWLbBHkgpeJwaJdLkD9n7zH\r\n"
			+ "wgrmcXFuqPuquS/IkSzDMkIpEy612KrSw1JdWYSn/hs5flybwEjp\r\n"
			+ "-----END RSA PRIVATE KEY-----";
}

/* Tarea 192 */
  /* Linea 1: Crear la llave secreta */
  /* Linea 2: Crear la llave SSL-RSA publica */
  /* Linea 3: Crear la llave SSL-RSA privada */
  
