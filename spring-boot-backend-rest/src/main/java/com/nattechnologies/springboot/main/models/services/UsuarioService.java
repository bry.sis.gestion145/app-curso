package com.nattechnologies.springboot.main.models.services;

import org.springframework.transaction.annotation.Transactional;

import com.nattechnologies.springboot.main.models.dao.IUsuarioDao;
import com.nattechnologies.springboot.main.models.entity.Usuario;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service /* Linea 2 */ /* Linea 1 */ /* Linea 4 */
public class UsuarioService implements IUsuarioService, UserDetailsService {

	private final Logger logger = LoggerFactory.getLogger(UsuarioService.class);
	
	@Autowired
	private IUsuarioDao usuarioDao;
	
	/* Linea 1 */
	@Override
	@Transactional(readOnly=true) /* Linea 3 */
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Usuario usuario = usuarioDao.findByUsername(username);
		
		if(usuario == null) {
			logger.error("Error en el login: no existe el usuario '"+ username +"' en el Sistema!!!");
			throw new UsernameNotFoundException("Error en el login: no existe el usuario '"+ username +"' en el Sistema!!!");
		}
		
		List<GrantedAuthority> authorities = usuario.getRoles()
				                                    .stream()
				                                    .peek(authority -> logger.info("Role: " + authority.getNombre()))
				                                    .map(role -> new SimpleGrantedAuthority(role.getNombre())
				                                    	/*{
				                                    	    return new SimpleGrantedAuthority(role.getNombre());
				                                    	}*/
				                                    ).collect(Collectors.toList());
		
		return new User(usuario.getUsername(), usuario.getPassword(), usuario.isEnabled(), true, true, true, authorities);
	}

	/* Linea 4 */
	@Override
	@Transactional(readOnly=true) 
	public Usuario findByUsername(String username) {
		return usuarioDao.findByUsername(username);
	}

}

/* Tarea 181 */
  /* Linea 1: Implementar la interfaz UserDetailsService y implementar los metodos del contrato */
  /* Linea 2: Anotar la clase como un service */
  /* Linea 3: Anotar el metodo con la anotacion @Transactional */

/* Tarea 198 */
  /* Linea 4: Implementar la interfaz UsuarioService y implementar los metodos del contrato */
