package com.nattechnologies.springboot.main.models.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/* Linea 1 */
@Entity
@Table(name = "usuarios")
public class Usuario implements Serializable {
	/* Linea 2 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/* Linea 2 */
	@Column(unique = true, length = 20)
	private String username;

	/* Linea 2 */
	@Column(length = 60)
	private String password;

	/* Linea 2 */
	private boolean enabled;
	
	private String nombre;
	private String apellido;
	
	@Column(unique = true)
	private String email;

	/* Linea 3 */
	@NotNull
	@ManyToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name="usuarios_roles", joinColumns=@JoinColumn(name="usuario_id"),
	inverseJoinColumns=@JoinColumn(name="role_id"),
	uniqueConstraints = {@UniqueConstraint(columnNames= {"usuario_id", "role_id"})})
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private List<Role> roles;

	private static final long serialVersionUID = 1L;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
}

/* Tarea 175 */
	/*
	 * Linea 1: Implementar de serializable y crear el atributo de la interfaz
	 * serializable Anotar la clase con @Entity y @Table
	 */
	/* Linea 2: Crear los atributos de la tabla y anotarlos */
    /* Linea 3: Establecer la relacion muchos a muchos entre los usuarios y los roles */