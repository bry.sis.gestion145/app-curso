package com.nattechnologies.springboot.main.auth;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Component;

import com.nattechnologies.springboot.main.models.entity.Usuario;
import com.nattechnologies.springboot.main.models.services.IUsuarioService;

/* Linea 1 */
@Component
public class InfoAdicionalToken implements TokenEnhancer {

	/* Linea 3 */
	@Autowired
	public IUsuarioService iUsuarioService;
	
	/* Linea 2 */
	@Override
	public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
		Usuario usuario = iUsuarioService.findByUsername(authentication.getName());
		Map<String, Object> info = new HashMap<>();
		info.put("info_adicional", "Hola que tal!: ".concat(authentication.getName()));
		info.put("id_usuario",       usuario.getId());
		info.put("usuario_acceso",   usuario.getUsername());
		info.put("usuario_acceso",   usuario.getPassword());
		info.put("nombre_usuario",   usuario.getNombre());
		info.put("apellido_usuario", usuario.getApellido());
		info.put("email_usuario",    usuario.getEmail());
		((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(info);
		return accessToken;
	}

}

/* Tarea 195 */
  /* Linea 1: Implementar de la interfaz TokenEnhacer y anotar la clase */
  /* Linea 2: Implementar los metodos de la interfaz TokenEnhacer */
  /* Linea 3: Inyectamos el IUsuarioService */