package com.nattechnologies.springboot.main.models.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.nattechnologies.springboot.main.models.entity.Usuario;

/* Linea 1 */
public interface IUsuarioDao extends CrudRepository<Usuario, Long> {
    
	/* Linea 2 */
	public Usuario findByUsername(String username);
	
	/* Linea 2 */
	@Query("select u from Usuario u where u.username=?1")
	public Usuario findByUsername2(String username);
	
	/* @Query("select u from Usuario u where u.username=?1 and u.otro=?2")
	public Usuario findByUsername3(String username, String otro); */
	
}

/* Tarea 179 */
	/* Linea 1: Hacemos que nuestra interface IUsuarioDao extendemos de CrudRepository<> */
	/* Linea 2: Implementar el metodo buscarUsuario */
