package com.nattechnologies.springboot.main;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Iterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.nattechnologies.springboot.main.auth.SpringSecurityConfig;
import com.nattechnologies.springboot.main.models.services.UploadFileServiceImpl;

@SpringBootApplication /* Linea 5 */
public class SpringBootBackendRestApplication implements CommandLineRunner {

	@Autowired /* Linea 7*/
	@Qualifier("bCryptPasswordEncoder")
	private BCryptPasswordEncoder passwordEncoder;
	
	@Autowired /* Linea 3 */
	private static UploadFileServiceImpl uploadFileServiceImpl;
	
	public static void main(String[] args) {
		SpringApplication.run(SpringBootBackendRestApplication.class, args);
	}
	
	@Override /* Linea 6 */
	public void run(String... args) throws Exception {
		/* Linea 2: llamada anterior */
	    /* limpiarUploads();         */
		/* Linea 4: llamada actual   */
		uploadFileServiceImpl.limpiarUploads();
	
		/* Generar contraseñas encriptadas */
		String password = "12345";
		for (int i = 0; i < 4; i++) {
			String passwordBcrypt = passwordEncoder.encode(password);
			System.out.println(passwordBcrypt);
		}
	}

	/* Linea 1: metodo anterior */ 
	/*  public static void limpiarUploads() {
			String sDirectorio = Paths.get("uploads").resolve("").toAbsolutePath().toString();
			File f = new File(sDirectorio);
					
			if (f.delete()) {
			    System.out.println("La carpeta " + sDirectorio + " ha sido borrado correctamente");
			    if (!f.exists()) {
		            if (f.mkdirs()) {
		            	System.out.println("La carpeta " + sDirectorio + " ha sido creado correctamente");
		            } else {
		            	System.out.println("La carpeta " + sDirectorio + " no se ha podido crear");
		            }
		        }
			} else {
			    System.out.println("La carpeta " + sDirectorio + " no se ha podido borrar");
			    File[] files = f.listFiles();
			    if(files.length > 0) {
			    	System.out.println("La carpeta " + sDirectorio + " contiene archivos ");
			    	for (int i = 0; i < files.length; i++) {
			    		String rutaArchivo = files[i].getAbsolutePath().toString();
			    		if(files[i].exists()) {
			    			if(files[i].delete()) {
			    				System.out.println("El archivo " + rutaArchivo + " ha sido borrado ");
			    			} else {
			    				System.out.println("El archivo " + rutaArchivo + " no se pudo borrar ");
			    			}
			    		} else {
			    			System.out.println("El archivo " + rutaArchivo + " no existe ");
			    		}
			    	}
			    }
			}
	    } */
	
}


/* Tarea 125 */
  /* Linea 1: Crear metodo para Eliminar las imagenes generadas en la anterior ejecucion del proyecto */
  /* Linea 2: Ejecutar el metodo que hace la limpieza en el metodo main */

/* Tarea 156 */
  /* Linea 3: Inyectar el servicio UploadFileServiceImpl como variable estatica porque vamos a llamar a su metodo estatico */
  /* Linea 4: Ejecutar en el main el metodo limpiarUploads() en el main */

/* Tarea 189 */
  /* Linea 5: Implementar de CommandLine */
  /* Linea 6: Implementar el metodo CommandLine */
  /* Linea 7: Inyectamos el BCryptPasswordEncoder */

/* ************************************************************************************************************** */
/* Procesos del Proyecto Completo */
/* Proceso 8: Primeros pasos */
  /* Tarea 38: Revisar Archivo pom.xml y ver sus dependencias */
  /* Tarea 39: Revisar Archivo src/main/resources/aplication.properties y agregar los atributos globales 
   * del proyecto */

/* Proceso 9: Crear los modelos */
  /* Tarea 40: Crear el paquete models.entity */
  /* Tarea 41: En el paquete models.entity Crear la clase Cliente */
  /* Tarea 42: Archivo models.entity => Cliente */

/* Proceso 10: Crear los repositorios */
  /* Tarea 43: Crear el paquete models.dao (Repository) */
  /* Tarea 44: En el paquete models.dao Crear la interfaz IClienteDao */
  /* Tarea 45: Archivo models.dao => IClienteDao */

/* Proceso 11: Crear los servicios */
  /* Tarea 46: Crear el paquete models.service */
  /* Tarea 47: En el paquete models.service Crear la interfaz IClienteService */
  /* Tarea 48: Archivo models.services => IClienteService */
  /* Tarea 49: En el paquete models.service Crear la clase ClienteServiceImpl */
  /* Tarea 50: Archivo models.services => ClienteServiceImpl */

/* Proceso 12: Crear los controladores */
  /* Tarea 51: Crear el paquete controllers */
  /* Tarea 52: En el paquete controllers Crear la clase ClienteRestController */
  /* Tarea 53: Archivo controllers => ClienteRestController */

/* Proceso 13: Añadir datos de prueba */
  /* Tarea 54: En el directorio src/main/resources crear el archivo import.sql */ 
  /* Tarea 55: Archivo src/main/resource => import.sql */ 

/* Proceso 14: Configurar CORS: Intercambio de Recursos de Origen Cruzado */
  /* Tarea 56: Archivo controllers => ClienteRestController */

/* Proceso 16: Creacion del CRUD */
  /* Tarea 59: Archivo models.services => IClienteService */
  /* Tarea 60: Archivo models.services => ClienteServiceImpl */
  /* Tarea 61: Archivo controllers => ClienteRestController */
  /* Tarea 62: Probar API con postman: https://www.udemy.com/course/angular-spring/learn/lecture/11992420#content */
  
/* Proceso 18: Manejo de errores */ 
  /* Tarea 86: Archivo controllers => ClienteRestController */
  /* Tarea 89: Archivo models.entity => Cliente */
  /* Tarea 90: Archivo controllers => ClienteRestController */
  /* Tarea 94: Archivo models.entity => Cliente */

/* Proceso 21: Implementacion de la consulta de Paginacion con Spring Data */ 
  /* Tarea 101: Archivo models.dao => IClienteDao */
  /* Tarea 102: Archivo models.services => IClienteService */
  /* Tarea 103: Archivo models.services => ClienteServiceImpl */
  /* Tarea 104: Archivo controllers => ClienteRestController */

/* Proceso 23: Preparacion del back-end para trabajar con el control de fechas */ 
  /* Tarea 116: Archivo models.entity => Cliente */
  /* Tarea 117: Archivo src/main/resources/aplication.properties configurar el locale */

/* Proceso 25: Trabajar con la subida de imagenes */ 
  /* Tarea 122: Archivo models.entity => Cliente */
  /* Tarea 123: Archivo controllers => ClienteRestController */
  /* Tarea 124: Archivo src/main/resources/aplication.properties y configurar parametros de la imagen */
  /* Tarea 125: Archivo main => SpringBootBackendRestAplication */
  /* Tarea 126: Archivo controllers => ClienteRestController */

/* Proceso 27: Crear Servicio UploadFile para manejar todo el codigo relacionado con la carga de la imagen */
  /* Tarea 151: En el paquete models.services crear la interfaz IUploadFileService */
  /* Tarea 152: En el paquete models.services crear la clase UploadFileServiceImpl */
  /* Tarea 153: Archivo models.services => IUploadFileService */
  /* Tarea 154: Archivo models.services => UploadFileServiceImpl */
  /* Tarea 155: Archivo controllers => ClienteRestController */
  /* Tarea 156: Archivo main => SpringBootBackendRestAplication */

/* Proceso 28: Trabajar con las regiones */
  /* Tarea 157: En el paquete models.entity crear la clase Region */
  /* Tarea 158: Archivo models.entity => Region */
  /* Tarea 159: Archivo models.entity => Cliente */
  /* Tarea 160: Archivo models.dao => IClienteDao */
  /* Tarea 161: Archivo models.services => IClienteService */
  /* Tarea 162: Archivo models.services => ClienteServiceImpl */
  /* Tarea 163: Archivo controllers => ClienteRestController */

/* Proceso 30: Trabajar con JWT desde el back-end */
  /* Tarea 172: Teoria JWT: https://www.udemy.com/course/angular-spring/learn/lecture/12470182#announcements
                Teoria JWT: https://www.udemy.com/course/angular-spring/learn/lecture/12474880#announcements */
  /* Tarea 173: Agregar las dependencias de JWT en el pom.xml */
  /* Tarea 174: En el paquete models.entity crear la clase Usuario y Role */
  /* Tarea 175: Archivo models.entity => Usuario */
  /* Tarea 176: Archivo models.entity => Role */
  /* Tarea 178: En el paquete models.dao crear la interfaz => IUsuarioDao */
  /* Tarea 179: Archivo models.dao => IUsuarioDao */
  /* Tarea 180: En el paquete models.service crear la clase UsuarioService */
  /* Tarea 181: Archivo models.service => UsuarioService */
  /* Tarea 182: Crear el paquete main.auth */
  /* Tarea 183: En el paquete main.auth crear la clase SpringSecurityConfig */
  /* Tarea 184: Archivo main.auth => SpringSecurityConfig */
  /* Tarea 185: En el paquete main.auth crear la clase AuthorizationServerConfig */
  /* Tarea 186: Archivo AuthorizationServerConfig */
  /* Tarea 187: En el paquete main.auth crear la clase ResourceServerConfig */
  /* Tarea 188: Archivo main.auth => ResourceServerConfig */
  /* Tarea 189: Archivo main => SpringBootBackendRestAplication */
  /* Tarea 190: Probar JWT: https://www.udemy.com/course/angular-spring/learn/lecture/12659790#announcements */
  /* Tarea 191: En el paquete main.auth crear la clase JwtConfig */
  /* Tarea 192: Archivo main.auth => JwtConfig */
  /* Tarea 193: Usar OpenSSL: https://www.udemy.com/course/angular-spring/learn/lecture/12669242#announcements */
  /* Tarea 194: En el paquete main.auth crear la clase JwtConfig InfoAdicionalToken */
  /* Tarea 195: Archivo main.auth => InfoAdicionalToken */
  /* Tarea 196: En el paquete models.service creamos la interfaz IUsuarioService */
  /* Tarea 197: Archivo models.service => IUsuarioService */
  /* Tarea 198: Archivo models.service => UsuarioService */
  /* Tarea 199: Archivo main.auth => AuthorizationServerConfig */
  /* Tarea 200: Archivo main.controllers => ClienteRestController */

/* Proceso 31: Configurar Cors */
  /* Tarea 201: Archivo main.auth => ResourceServerConfig */

/* Proceso 33: Trabajar con la facturacion del cliente */
  /* Tarea 236: Crear la clase Producto */
  /* Tarea 237: Archivo models.entity => Producto */
  /* Tarea 238: Crear la clase ItemFactura */
  /* Tarea 239: Archivo models.entity => ItemFactura */
  /* Tarea 240: Crear la clase Factura */
  /* Tarea 241: Archivo models.entity => Factura */

/* Proceso 34: Trabajar con la logica de negocio de las facturas */
  /* Tarea 241B: Crear el campo factura en la entidad clientes */
  /* Tarea 242: Crear la clase IFacturaDao*/
  /* Tarea 243: Archivo models.dao => IFacturaDao */
  /* Tarea 244: Crear la clase IProductoDao */
  /* Tarea 245: Archivo models.dao => IProductoDao */
  /* Tarea 246: Archivo models.service => IClienteService */
  /* Tarea 247: Archivo models.service => ClienteServiceImpl */
  /* Tarea 248: Archivo main.controllers => FacturaRestController */

/* Proceso 36: Preparrando el Back-End para el Deploy */
  /* Tarea 249: Archivo main.controllers => FacturaRestController */
  /* Tarea 250: Archivo main.controllers => ClienteRestController */
  /* Tarea 251: Archivo main.controllers => ResourceServerConfig */ 
  /* Tarea 552: Archivo src/main/resource => import.sql */ 
/* ************************************************************************************************************** */
