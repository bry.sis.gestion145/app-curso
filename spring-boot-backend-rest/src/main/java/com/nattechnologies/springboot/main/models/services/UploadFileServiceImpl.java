package com.nattechnologies.springboot.main.models.services;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service /* Linea 1 */
public class UploadFileServiceImpl implements IUploadFileService {

	/* Linea 3 */
	private final Logger log = LoggerFactory.getLogger(UploadFileServiceImpl.class);
	
	/* Linea 4 */
	private final static String DIRECTORIO_UPLOAD = "uploads";
	
	@Override /* Linea 2 */ /* Linea 5 */
	public Path getPath(String nombreFoto) {
		return Paths.get(DIRECTORIO_UPLOAD).resolve(nombreFoto).toAbsolutePath();
	}
	
	@Override /* Linea 2 */ /* Linea 6 */
	public Resource cargar(String nombreFoto) throws MalformedURLException {
		Path rutaArchivo = getPath(nombreFoto);
		log.info(rutaArchivo.toString());
		
		Resource recurso = new UrlResource(rutaArchivo.toUri());

		if(!recurso.exists() && !recurso.isReadable()) {
			rutaArchivo = Paths.get("src/main/resources/static/images").resolve("no-usuario.png").toAbsolutePath();
			recurso = new UrlResource(rutaArchivo.toUri());
			log.error("Error no se pudo cargar la imagen: " + nombreFoto);
		}
		return recurso;
	}

	@Override /* Linea 2 */ /* Linea 7 */
	public String guardar(MultipartFile archivo) throws IOException {
		String nombreArchivo = UUID.randomUUID().toString() + "__" + archivo.getOriginalFilename().replace(" ", "");
		Path rutaArchivo = getPath(nombreArchivo);
		// Linea 23 
		log.info(rutaArchivo.toString());
		
		Files.copy(archivo.getInputStream(), rutaArchivo);
			
		return nombreArchivo;
	}

	@Override /* Linea 2 */ /* Linea 8 */
	public boolean eliminar(String nombreFotoAnterior) {
		if(nombreFotoAnterior != null && nombreFotoAnterior.length() > 0) {
			   Path rutaFotoAnterior = Paths.get("uploads").resolve(nombreFotoAnterior).toAbsolutePath();
			   File archivoFotoAnterior = rutaFotoAnterior.toFile();
			   if(archivoFotoAnterior.exists() && archivoFotoAnterior.canRead()) {
				   archivoFotoAnterior.delete();
				   return true;
			   }
			}
		return false;
	}
	
	/* Linea 9 */
	public static void limpiarUploads() {
		String sDirectorio = Paths.get("uploads").resolve("").toAbsolutePath().toString();
		File f = new File(sDirectorio);
				
		if (f.delete()) {
		    System.out.println("La carpeta " + sDirectorio + " ha sido borrado correctamente");
		    if (!f.exists()) {
	            if (f.mkdirs()) {
	            	System.out.println("La carpeta " + sDirectorio + " ha sido creado correctamente");
	            } else {
	            	System.out.println("La carpeta " + sDirectorio + " no se ha podido crear");
	            }
	        }
		} else {
		    System.out.println("La carpeta " + sDirectorio + " no se ha podido borrar");
		    File[] files = f.listFiles();
		    if(files.length > 0) {
		    	System.out.println("La carpeta " + sDirectorio + " contiene archivos ");
		    	for (int i = 0; i < files.length; i++) {
		    		String rutaArchivo = files[i].getAbsolutePath().toString();
		    		if(files[i].exists()) {
		    			if(files[i].delete()) {
		    				System.out.println("El archivo " + rutaArchivo + " ha sido borrado ");
		    			} else {
		    				System.out.println("El archivo " + rutaArchivo + " no se pudo borrar ");
		    			}
		    		} else {
		    			System.out.println("El archivo " + rutaArchivo + " no existe ");
		    		}
		    	}
		    }
		}
	}
	
}

/* Tarea 154 */
  /* Linea 1: Hacer que nuestra clase implemente de la interfaz IUploadFileService, anotarla con @Service */
  /* Linea 2: Implementar los metodos de la interfaz que estamos implementando en nuestra clase. */
  /* Linea 3: Crear el atributo log */
  /* Linea 4: Crear la constante que contiene la direccion de la carpeta uploads */
  /* Linea 5: Imnplementar el metodo getPath() */
  /* Linea 6: Imnplementar el metodo cargar() */
  /* Linea 7: Imnplementar el metodo copiar() o guardar() */
  /* Linea 8: Imnplementar el metodo eliminar() */
  /* Linea 9: Implementar el metodo de limpieza de la carpeta Uploads  */
