package com.nattechnologies.springboot.main.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.nattechnologies.springboot.main.models.entity.Factura;

/* Tarea 243 */
public interface IFacturaDao extends CrudRepository<Factura, Long>{

}

/* Tarea 243 */
   /* Linea 1: Implementar de IFacturaDao */