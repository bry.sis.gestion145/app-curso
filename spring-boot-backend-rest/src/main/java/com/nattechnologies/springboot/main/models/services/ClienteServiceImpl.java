package com.nattechnologies.springboot.main.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nattechnologies.springboot.main.models.dao.IFacturaDao;
import com.nattechnologies.springboot.main.models.dao.IProductoDao;
import com.nattechnologies.springboot.main.models.dao.IClienteDao;
import com.nattechnologies.springboot.main.models.entity.Cliente;
import com.nattechnologies.springboot.main.models.entity.Factura;
import com.nattechnologies.springboot.main.models.entity.Producto;
import com.nattechnologies.springboot.main.models.entity.Region;

/* Linea 2 */
@Service
public class ClienteServiceImpl implements IClienteService {
/* Linea 1 */
	
	/* Linea 3 */
	@Autowired
	private IClienteDao clienteDao; 
	
	/* Linea 9 */
	@Autowired
	private IFacturaDao facturaDao;
	@Autowired
	private IProductoDao productoDao;
	
	
	/* Linea 5 */
	@Override
	@Transactional(readOnly = true)
	public List<Cliente> findAll() {	
		/* Linea 4 */
		return (List<Cliente>) clienteDao.findAll();
	}
	
	/* Linea 7 */
	@Override
	@Transactional(readOnly = true)
	public Page<Cliente> findAll(Pageable pageable) {
		return clienteDao.findAll(pageable);
	}

	/* Linea 6 */
	@Override
	@Transactional(readOnly = true)
	public Cliente findById(Long id) {
		return clienteDao.findById(id).orElse(null);
	}
	@Override
	@Transactional
	public Cliente save(Cliente cliente) {
		return clienteDao.save(cliente);
	}
	@Override
	@Transactional
	public void delete(Long id) {
		clienteDao.deleteById(id);
	}
	
	/* Linea 8 */
	@Override
	@Transactional(readOnly = true)
	public List<Region> findAllRegiones(){
	    return clienteDao.findAllRegiones();   	
	}

	/* Linea 10 */
	@Override
	@Transactional(readOnly = true)
	public Factura findFacturaById(Long id) {
		return facturaDao.findById(id).orElse(null);
	}

	/* Linea 10 */
	@Override
	@Transactional
	public Factura saveFactura(Factura factura) {
		return facturaDao.save(factura);
	}

	/* Linea 10 */
	@Override
	@Transactional
	public void deleteFacturaById(Long id) {
		facturaDao.deleteById(id);
	}

	/* Linea 10 */
	@Override
	@Transactional(readOnly = true)
	public List<Producto> findProductoByNombre(String term) {
		return productoDao.findByNombreContainingIgnoreCase(term);
	}

	
}

/* Tarea 50 */
  /* Linea 1: Implementar la interfaz IClienteService */
  /* Linea 2: Anotar la clase con la anotacion Service */
  /* Linea 3: Inyectar el clientedao */
  /* Linea 4: Implementar el retorno del metodo findAll() */
  /* Linea 5: Anotar el metodo listar con la anotacion Transactional */

/* Tarea 60 */
  /* Linea 6: Implementar metodos del CRUD */

/* Tarea 103 */
  /* Linea 7: Implementar el metodo de paginacion de la interfaz */

/* Tarea 162 */
  /* Linea 8: Implementamos el metodo mostrar regiones */

/* Tarea 247A */
  /* Linea 9: Implementar los dao de las facturas y productos*/
  /* Linea 10: Implementar los metodos de las facturas*/