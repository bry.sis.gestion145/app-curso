package com.nattechnologies.springboot.main.models.services;

import com.nattechnologies.springboot.main.models.entity.Usuario;

public interface IUsuarioService {

	/* Linea 1 */
	public Usuario findByUsername(String username);
	
}

/* Tarea 197 */
  /* Linea 1: Crear el metodo encontrar nombre de usuario */