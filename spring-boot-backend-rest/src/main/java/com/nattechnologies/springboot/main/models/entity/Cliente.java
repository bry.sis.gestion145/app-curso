package com.nattechnologies.springboot.main.models.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
// import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.nattechnologies.springboot.main.models.entity.Factura;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties; 

/* Linea 5 */
@Entity 
@Table(name="clientes")
public class Cliente implements Serializable {
	/* Linea 3 */	

	/* Linea 6 */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	/* Linea 9 */
	@NotEmpty(message = "no debe estar vacío") /* Linea 8 */
	@Size(min=4, max=16) /* Linea 8 */
	@Column(nullable=false) 
	private String nombre;
	
	@NotEmpty /* Linea 8 */
	@Size(min=4, max=16) /* Linea 8 */
	private String apellido;
	
	@NotEmpty /* Linea 8 */
	@Email
	@Column(nullable=false, unique=true)
	private String email;
	
	@NotNull /* Linea 10 */
	@Column(name="create_at")
	@Temporal(TemporalType.DATE)
	private Date createAt;
	/* Linea 1 */
	
	/* Linea 11 */
	private String foto;
	
	/* Linea 12 */
	@NotNull
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="region_id")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private Region region;
	
	/* LInea 13 */
	@JsonIgnoreProperties(value={"cliente", "hibernateLazyInitializer", "handler"}, allowSetters=true)
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "cliente", cascade = CascadeType.ALL)
	private List<Factura> facturas;
    
	public Cliente() {
		this.facturas = new ArrayList<>();
	}
	
	/* Linea 2B */ /* Linea 10 */
	/* @PrePersist
	   public void prePersist() {
		   createAt = new Date();
	   } */

	/* Linea 2A */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}
	
	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}
	
	public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}
	
	public List<Factura> getFacturas() {
		return facturas;
	}

	public void setFacturas(List<Factura> facturas) {
		this.facturas = facturas;
	}
	
	/* Linea 4 */
	private static final long serialVersionUID = 1L;
	
}

/* Tarea 42 */
  /* Linea 1: Crear atributos del modelo cliente */
  /* Linea 2A: Crear los get y los sets de los atributos */
  /* Linea 2B: Crear el metodo prePersist */

/* Convertir la clase en una entidad */
  /* Linea 3: Implemetar la interfaz serializable */
  /* Linea 4: Crear el id de la clase serializable */
  /* Linea 5: Decorar la clase con la anotacion entity y la anotacion table */
  /* Linea 6: Anotar el campo id con anotacion id y la anotacion generatedvalue */
  /* Linea 7: Anotar el campo createAt con la anotacion column y la anotacion temporal */

/* Tarea 89 */
  /* Linea 8: Agregar reglas de validacion en los atributos */

/* Tarea 94 */
  /* Linea 9: Personalizar los mensajes de las reglas de validacion con la propiedad message */

/* Tarea 116 */
  /* Linea 10: Anotamos el campo createAt con la anotacion @NotNull y revisar si esta implementado 
     el metodo con la anotacion @PrePersist para la fecha, el cual vamos a quitar */

/* Tarea 122 */
  /* Linea 11: Creamos el campo foto para la imagen y creamos los get y los set */

/* Tarea 159 */
  /* Linea 12: Crear atributo region con sus gets y sets creamos la anotacion con la cual declararemos la
               relacion entre clientes y regiones */

/* Tarea 241B */
   /* Linea 13: Crear atributo facturas */
