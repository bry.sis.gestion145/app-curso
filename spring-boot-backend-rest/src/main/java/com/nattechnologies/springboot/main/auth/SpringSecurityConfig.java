package com.nattechnologies.springboot.main.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.nattechnologies.springboot.main.models.services.UsuarioService;

/* Linea 1 */
@Configuration /* Linea 2 */
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled=true)
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired /* Linea 3 */
	private UsuarioService usuarioService;
	
	@Bean("bCryptPasswordEncoder") /* Linea 4 */
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Override 
	@Autowired /* Linea 5 */
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(this.usuarioService).passwordEncoder(passwordEncoder());
	}
	
	@Bean("authenticationManager")
	@Override /* Linea 6 */
	protected AuthenticationManager authenticationManager() throws Exception {
		return super.authenticationManager();
	}
	
	@Override /* Linea 5 */
	public void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
		.anyRequest().authenticated()
		.and()
		.csrf().disable()
		.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	}
	
}

/* Tarea 184 */
  /* Linea 1: Anotar la clase con la anotacion de configuracion */
  /* Linea 2: Heredamos la clase de la clase WebSecurityAdapter */
  /* Linea 3: Inyectamos el atributo usuarioService de clase UserDetailsService*/
  /* Linea 4: Creamos el metodo passwordEncoder() y lo anotamos para registrar su retorno como un bean */
  /* Linea 5: Sobre escribimos el metodo configure() */
  /* Linea 6: Sobre escribimos el metodo authenticationManager() */
  /* Linea 7: Habilitar la anotacion de autenticacion */