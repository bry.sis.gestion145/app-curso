package com.nattechnologies.springboot.main.models.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.nattechnologies.springboot.main.models.entity.Cliente;
import com.nattechnologies.springboot.main.models.entity.Region;

/* Linea 1 */
// public interface IClienteDao extends CrudRepository<Cliente, Long> {
/* Linea 2 */
public interface IClienteDao extends JpaRepository<Cliente, Long> {	

   /* Linea 3 */
   @Query("from Region") 
   public List<Region> findAllRegiones();	
   /* Region es el nombre de la clase no de la tabla */	
   
}

/* Tarea 45 */
  /* Linea 1: Heredar de la clase CrudRepository */

/* Tarea 101 */
  /* Linea 2: Cambiar CrudRepository por JpaRepository */

/* Tarea 161 */
  /* Linea 3: Crear el metodo para listar las regiones */