package com.nattechnologies.springboot.main.models.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/* Linea 1 */
@Entity
@Table(name = "roles")
public class Role implements Serializable {
	/* Linea 2 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(unique = true, length = 20)
	private String nombre; 

	private static final long serialVersionUID = 1L;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}

/* Tarea 176 */
	/*
	 * Linea 1: Imlementar de serializable y crear el atributo de la interfaz
	 * serializable Anotar la clase con @Entity y @Table
	 */
	/* Linea 2: Crear los atributos de la tabla y anotarlos */

	/* Linea 3: Establecer la relacion muchos a muchos entre los usuarios y los roles */
