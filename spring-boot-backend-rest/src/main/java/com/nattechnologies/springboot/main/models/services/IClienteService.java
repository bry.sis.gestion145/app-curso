package com.nattechnologies.springboot.main.models.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.nattechnologies.springboot.main.models.entity.Factura;
import com.nattechnologies.springboot.main.models.entity.Producto;
import com.nattechnologies.springboot.main.models.entity.Cliente;
import com.nattechnologies.springboot.main.models.entity.Region;

public interface IClienteService {

	/* Linea 1 */
	public List<Cliente> findAll();
	
	/* Linea 3 */
	public Page<Cliente> findAll(Pageable pageable);

	/* Linea 2 */
	public Cliente findById(Long id);
	public Cliente save(Cliente cliente);
	public void delete(Long id);
	
	/* Linea 4 */
	public List<Region> findAllRegiones();
	
	/* Linea 5 */
    public Factura findFacturaById(Long id);
	public Factura saveFactura(Factura factura);
	public void deleteFacturaById(Long id);
	public List<Producto> findProductoByNombre(String term);
	
}

/* Tarea 48 */
  /* Linea 1: Crear el metodo listar */

/* Tarea 59 */
  /* Linea 2: Crear los metodos del CRUD */

/* Tarea 102 */
  /* Linea 3: Creamos otro metodo findAll() pero de paginacion */

/* Tarea 161 */
  /* Linea 4: Traemos el metodo mostrar regiones */

/* Tarea 246 */
  /* Linea 5: Crear los metodos service de las facturas */
