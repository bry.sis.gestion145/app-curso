package com.nattechnologies.springboot.main.models.services;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Path;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface IUploadFileService {
    
	/* Linea 1 */
	public Path getPath(String nombreFoto);
	
	/* Linea 2 */
	public Resource cargar(String nombreFoto) throws MalformedURLException;
	
	/* Linea 3 */
	public String guardar(MultipartFile archivo) throws IOException;
	
	/* Linea 4 */
	public boolean eliminar(String nombreFoto);
	
}

/* Tarea 153 */
  /* Linea 1: Implementar el metodo getPath() para obtener la url */
  /* Linea 2: Implementar el metodo cargar() imagen */
  /* Linea 3: Implementar el metodo copiar() o guardar() imagen */
  /* Linea 4: Implementar el metodo eliminar() imagen */
  