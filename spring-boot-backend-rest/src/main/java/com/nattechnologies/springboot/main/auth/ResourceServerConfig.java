package com.nattechnologies.springboot.main.auth;

import java.util.Arrays;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Configuration /* Linea 1 */
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

	/* Linea 2 */
	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
		.antMatchers("/api/clientes/{id}").permitAll()
		.antMatchers("/api/facturas/{id}").permitAll()
		.antMatchers("/api/facturas/**").permitAll()
		.antMatchers(HttpMethod.GET, "/api/clientes").permitAll()
		.antMatchers(HttpMethod.GET, "/api/clientes/page/**").permitAll()
		.antMatchers(HttpMethod.GET, "/api/uploads/img/**").permitAll()
		.antMatchers(HttpMethod.GET, "/images/**").permitAll()
	  //.antMatchers(HttpMethod.GET, "/api/clientes/{id}").hasAnyRole("USER", "ADMIN")
	  //.antMatchers(HttpMethod.POST, "/api/clientes/upload").hasAnyRole("USER", "ADMIN")
	  //.antMatchers(HttpMethod.POST, "/api/clientes").hasRole("ADMIN")
	  //.antMatchers("/api/clientes/**").hasRole("ADMIN")
		.anyRequest().authenticated()
		.and().cors().configurationSource(corsConfigurationSource()); /* Linea 4 */

	}
	
	/* Linea 6 */
	@Bean
	public CorsConfigurationSource corsConfigurationSource() {
		CorsConfiguration config = new CorsConfiguration();
		
		/* Linea 7 */
		// config.setAllowedOrigins(Arrays.asList("http://localhost:4200", "*"));
		config.setAllowedOrigins(Arrays.asList("http://localhost:4200"));
		config.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE", "OPTIONS"));
		config.setAllowCredentials(true);
		config.setAllowedHeaders(Arrays.asList("Content-Type", "Authorization"));
		
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", config);
		return source;
	}
	
	/* Linea 5 */
	@Bean
	public FilterRegistrationBean<CorsFilter> corsFilter(){
		FilterRegistrationBean<CorsFilter> bean = new FilterRegistrationBean<CorsFilter>(new CorsFilter(corsConfigurationSource()));
		bean.setOrder(Ordered.HIGHEST_PRECEDENCE);
		return bean;
	}

}

/* Tarea 188 */
  /* Linea 1: Anotamos la clase con configuraction, y extendemos de la clase ResourceServerConfiguredAdapter */
  /* Linea 2: Implementamos el metodo configure */

/* Tarea 201 */
  /* Linea 3: Crear metodo de configuracion de cors */
  /* Linea 4: Pasar como argumento el metodo configurado */
  /* Linea 5: Crear el metodo para registrar el bean */
  /* Linea 6: Implementamos el metodo corsConfigurationSource */

/* Tarea 251 */
  /* Linea 7: Agregar ruta, nombre de dominio o servidor, nombre de IP, si ponemos * se acepta cualquier ruta pero lo recomendable es:
              colocar el nombre del servidor o la IP que corresponda, esta misma ruta va en los controladores */