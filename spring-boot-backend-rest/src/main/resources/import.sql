/* Roles */
INSERT INTO `roles` (nombre) VALUES ('ROLE_ADMIN');
INSERT INTO `roles` (nombre) VALUES ('ROLE_USER');

/* Usuarios */
INSERT INTO `usuarios` (username, password, enabled, nombre, apellido, email) VALUES ('admin', '$2a$10$HLUKgCf.IQpwNa7ryDT51OmK94Fm5tFbMk0XbgtnuD0Jf5KlXlkRe', 1, 'Juan', 'Vega', 'admin@gmail.com');
INSERT INTO `usuarios` (username, password, enabled, nombre, apellido, email) VALUES ('alex', '$2a$10$HLUKgCf.IQpwNa7ryDT51OmK94Fm5tFbMk0XbgtnuD0Jf5KlXlkRe', 1, 'Alex', 'Gonzalez', 'alex@gmail.com');

/* Asignar Roles a Usuarios */
INSERT INTO `usuarios_roles` (usuario_id, role_id) VALUES (1, 1);
INSERT INTO `usuarios_roles` (usuario_id, role_id) VALUES (1, 2);
INSERT INTO `usuarios_roles` (usuario_id, role_id) VALUES (2, 2);

/* Regiones */
INSERT INTO regiones (id, nombre) VALUES (1, 'Sudam�rica');
INSERT INTO regiones (id, nombre) VALUES (2, 'Centroam�rica');
INSERT INTO regiones (id, nombre) VALUES (3, 'Norteam�rica');
INSERT INTO regiones (id, nombre) VALUES (4, 'Europa');
INSERT INTO regiones (id, nombre) VALUES (5, 'Asia');
INSERT INTO regiones (id, nombre) VALUES (6, '�frica');
INSERT INTO regiones (id, nombre) VALUES (7, 'Ocean�a');
INSERT INTO regiones (id, nombre) VALUES (8, 'Ant�rtida');

/* Personas Conocidas */
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES (1, 'Bryan',   'Gonzalez', 'bry.sis.gestion145@gmail.com', '2021-07-16');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES (2, 'Jose',    'Briones',  'jose.briones@gmail.com',       '2021-07-15');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES (3, 'Carlos',  'Orellana', 'carlos.orellana@gmail.com',    '2021-07-14');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES (4, 'Yulissa', 'Flores',   'yulissa.flores123@gmail.com',  '2021-07-13');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES (5, 'Nazaret', 'Rivas',    'nazaret.rivas856@gmail.com',   '2021-07-12');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES (6, 'Daniela', 'Torres',   'daniela.torres@gmail.com',     '2021-07-11');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES (7, 'Jovany',  'Torres',   'jovany.torres@gmail.com',      '2021-07-10');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES (8, 'Dinia',   'Torres',   'dinia.torres456@gmail.com',    '2021-07-09');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES (8, 'Ana',     'Obando',   'ana.ovando678@gmail.com',      '2021-07-08');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES (7, 'Alexis',  'Ovando',   'alexis.ovando@gmail.com',      '2021-07-07');

/* Personajes de Naruto Estudiantes */
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES (6, 'Naruto',    'Uzumaki',  'naruto.uzumaki@gmail.com', '2021-07-17');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES (5, 'Sakura',    'Haruno',   'sakura.haruno@gmail.com',  '2021-07-18');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES (4, 'Sasuke',    'Uchiha',   'sasuke.uchiha@gmail.com',  '2021-07-19');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES (3, 'Hinata',    'Hyuga',    'hinata.hyuga@gmail.com',   '2021-07-20');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES (2, 'Kiba',      'Inuzuka',  'kiba.inuzuka@gmail.com',   '2021-07-21');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES (1, 'Shino',     'Aburame',  'shino.aburame@gmail.com',  '2021-07-22');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES (2, 'Ino',       'Yamanaka', 'ino.yamanaka@gmail.com',   '2021-07-23');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES (4, 'Shikamaru', 'Nara',     'shikamaru.nara@gmail.com', '2021-07-24');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES (6, 'Choji',     'Akimichi', 'choji.akimichi@gmail.com', '2021-07-25');

/* Personajes de Naruto Maestros */
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES (8, 'Kakashi',   'Hatake',   'kakashi.hatake@gmail.com',   '2021-07-26');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES (7, 'Asuma',     'Sarutobi', 'asuma.sarutobi@gmail.com',   '2021-07-27');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES (5, 'Kurenai',   'Yuhi',     'kurenai.yuhi@gmail.com',     '2021-07-28');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES (3, 'Might',     'Guy',      'migth.guy@gmail.com',        '2021-07-29');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES (1, 'Jiraiya',   'Sanin',    'jiraiya.sanin@gmail.com',    '2021-07-30');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES (4, 'Orochimru', 'Sanin',    'orochimaru.sanin@gmail.com', '2021-08-01');
INSERT INTO clientes (region_id, nombre, apellido, email, create_at) VALUES (7, 'Tsunade',   'Sanin',    'tsunade.sanin@gmail.com',    '2021-08-02');

/* Productos */
INSERT INTO productos (nombre, precio, create_at) VALUES('Panasonic Pantalla LCD', 259990, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES('Sony Camara digital DSC-W320B', 123490, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES('Apple iPod shuffle', 1499990, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES('Sony Notebook Z110', 37990, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES('Hewlett Packard Multifuncional F2280', 69990, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES('Bianchi Bicicleta Aro 26', 69990, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES('Mica Comoda 5 Cajones', 299990, NOW());

/* Facturas */
INSERT INTO facturas (descripcion, observacion, cliente_id, create_at) VALUES('Factura equipos de oficina', null, 1, NOW());

INSERT INTO facturas_items (cantidad, factura_id, producto_id) VALUES(1, 1, 1);
INSERT INTO facturas_items (cantidad, factura_id, producto_id) VALUES(2, 1, 4);
INSERT INTO facturas_items (cantidad, factura_id, producto_id) VALUES(1, 1, 5);
INSERT INTO facturas_items (cantidad, factura_id, producto_id) VALUES(1, 1, 7);

INSERT INTO facturas (descripcion, observacion, cliente_id, create_at) VALUES('Factura Bicicleta', 'Alguna nota importante!', 1, NOW());
INSERT INTO facturas_items (cantidad, factura_id, producto_id) VALUES(3, 2, 6);

/* Tarea 55 */
  /* Linea 1: Agregar los datos de prueba */

/* Tarea 255 */
  /* Linea 2: Solucionar conflictos con los juegos de caractares */
/*
/* Pasos.
/* 1. Hacer modificaciones en el ResourceServerConfig y en los controladores ClienteRestController y FacturaRestController.*/
/*    2. Hacer modificaciones del "Formato de codificacion de caracteres" del el Import.sql              */
/*    2.1 Nos vamos a las propiedades del archivo.                                                       */
/*    2.2 Nos vamos a la seccion "Text file encoding".                                                   */
/*    2.3 Le damos click en Other (Otro).                                                                */
/*    2.4 Seleccionamos "ISO-8859-1".                                                                    */
/*    2.5 Damos click en el Apply (Aplicar) de arriba.                                                   */
/*    2.6 Damos click en el Apply and Close (Aplicar y cerrar) de abajo.                                 */
/* 3. El texto del Import.sql todos los acentos y las enies se modifican ahora habra que arreglarlos. */



